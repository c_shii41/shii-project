#include <stdio.h>
#include <math.h>
#define EPS 2e-3
int main(void)
{
  double x, x1, x2;
  x1 = 0;
  x2 = 1;
  while(x2 - x1 > EPS){
  x = (x1 + x2) / 2;
  if(cos(x) - x > 0)
  x2 = x;
  printf("x = %6.5f\n", (x1 + x2) / 2);
return 0; }
}
