#include <stdio.h>

int main()
{
  int fds[2], status;

  pipe(fds);

  if (fork()) {
    if (fork()) { 
      close(fds[0]); 
      close(fds[1]); 
      wait(&status); 
      wait(&status);
     } else {
      close(0); 
      dup(fds[0]);
      close(fds[0]);
      close(fds[1]);
      execl("/usr/bin/wc", "wc", NULL);
    }
  } else {
     close(1);
     dup(fds[1]);
     close(fds[0]);
     close(fds[1]);
     execl("/bin/ls", "ls", "-aFl", "/usr/bin", NULL); 
  }
  return 0;
} 