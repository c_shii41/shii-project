############################
############################
##    EEW Bot in Twitter  ##
## Toshiki Nakatsuru 2017 ##
##      WeatherNews       ##
############################
############################

##############################
# Importing a including data #
##############################
from datetime import datetime
from bs4 import BeautifulSoup
import tweepy
import socket
import struct
import config
import sys

#########
# Reset #
#########
max_quake = '最大震度 ６弱'
guess_quake = '推定震度 １未満'

###############
# Twitter API #
###############
CK = config.CONSUMER_KEY
CS = config.CONSUMER_SECRET
AT = config.ACCESS_TOKEN
AS = config.ACCESS_TOKEN_SECRET

auth = tweepy.OAuthHandler(CK, CS)
auth.set_access_token(AT, AS)
api = tweepy.API(auth)

#######################
# Set adress and port #
#######################
multicast_group = '234.211.239.231'
multicast_port = 65481
my_add = '192.168.10.7'

#########################
# Define server address #
#########################
server_address = (my_add, multicast_port)

#####################
# Create the socket #
#####################
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

##############################
# Bind to the server address #
##############################
sock.bind(server_address)

group = socket.inet_aton(multicast_group)
mreq = struct.pack('4sL', group, socket.INADDR_ANY)
sock.setsockopt(socket.IPPROTO_IP, socket.IP_ADD_MEMBERSHIP, mreq)

######################
# Streaming API main #
######################
while True:
    # update data every streaming
    data, address = sock.recvfrom(10240)

    # open The Lsat 10-Seconds's data
    with open('C:/Users/cente/AppData/Local/WEATHERNEWS INC/The Last 10-Second/flash/xml/rt_eq.xml', 'r') as doc:
        soup = BeautifulSoup(doc, 'xml')

    max_quake_old = max_quake
    guess_quake_old = guess_quake

    # Import some data
    # 第n報
    report_no = soup.find('report_no').string
    report_no = int(report_no)
    # 最終報
    status = soup.find('status').string
    status = int(status)
    # キャンセルフラグ
    cancel_flag = soup.find('cancel_flag').string
    cancel_flag = int(cancel_flag)
    # 地震ID
    eq_id = soup.find('eq_id').string
    # 震央名
    center_name = soup.find('center_name').string
    # 深さ
    depth = soup.find('depth').string
    # マグニチュード
    magnitude = soup.find('magnitude').string
    # 最大震度
    max_quake = soup.find('max_quake').string
    # 現在地の予想震度
    guess_quake = soup.find('guess_quake').string
    # 津波情報
    tsunami_potential = soup.find('tsunami_potential').string
    tsunami_potential = int(tsunami_potential)
    tsunami_str = '津波に関する情報はありません。'
    #津波情報文作成
    if (tsunami_potential == 1):
        tsunami_str = '念のため海岸から離れてください。¥n'

    # 発生時間
    ot1 = soup.find('obs_time').string
    ot2 = datetime.strptime(ot1, '%Y%m%d%H%M%S')
    ot_h = str(ot2.hour)
    ot_m = str(ot2.minute)
    ot_s = str(ot2.second)
    obs_time = ot_h + '時' + ot_m + '分' + ot_s + '秒'

    # 到達時間
    at1 = soup.find('arrive_time').string
    at2 = datetime.strptime(at1, '%Y%m%d%H%M%S')
    at_h = str(at2.hour)
    at_m = str(at2.minute)
    at_s = str(at2.second)
    arrive_time = at_h + '時' + at_m + '分' + at_s + '秒'

    data_change_flag = 0
    up_d = ''
    if (max_quake != max_quake_old or guess_quake != guess_quake_old):
        if (report_no != 1):
            data_change_flag = 1
            up_d = '(↑↓)'

    #【緊急地震速報（第13報）】
    str1 = '【緊急地震速報 第' + str(report_no) + '報】' + up_d + '\n'
    if (status):
        str1 = '【緊急地震速報 最終' + str(report_no) + '報】\n'
    # 発生時刻 12:34:56
    str2 = '発生時刻 ' + obs_time + '\n'
    # 震央 熊本県熊本地方 深さ 10km
    str3 = '震央:' + center_name + ' 深さ' + depth + 'km\n'
    # 最大震度　６弱
    str4 = max_quake + '　規模M' + magnitude + '\n'
    # 推定震度　５弱　（飯塚）
    str5 = '\n' + guess_quake + '(飯塚)\n'
    # 到達時刻  12:35:14頃
    str6 = '到達時刻 ' + arrive_time + '\n'
    #津波に関する情報
    str7 = '津波情報'

    max_quake_flag = 1
    if (max_quake == '最大震度 １'):
        max_quake_flag = 0
    if (max_quake == '最大震度 ２'):
        max_quake_flag = 0

    here_quake_flag = 1
    if (guess_quake == '推定震度 １未満'):
        str5 = ''
        str6 = ''
        here_quake_flag = 0

    if (tsunami_potential > 0):
        str7 = tsunami_str
    else:
        str7 = ''

    str_all = str1 + str2 + str3 + str4 + str5 + str6 + str7
    print(str_all)

    if (cancel_flag):
        str_all = '先程の緊急地震速報は取り消されました。'
        api.update_status(status=str_all)  
        print(str_all)

    if (here_quake_flag == 1 or max_quake_flag == 1):
        if (report_no == 1):
            api.update_status(status=str_all)  
            print ('第1報のためツイート\n')
    
        elif (report_no == 3):
            api.update_status(status=str_all)  
            print ('第3報のためツイート\n')

        elif (data_change_flag):
            api.update_status(status=str_all)  
            print ('震度情報更新のためツイート\n')
    
        elif (status):
            api.update_status(status=str_all)
            print ('最終報のためツイート\n')

        else:
            print('ツイート条件に一致しませんでした\n')

    else:
        print('ツイート条件に一致しませんでした(震度条件)')

    print('-------------------------------------\n')

    if (status):
        print('-------------------------------------\n')