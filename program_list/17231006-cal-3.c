/*
[課題1]
1.自分の誕生日の曜日を求める。
  誕生日を入力すると、生まれてから今日までの日数および誕生日の曜日を求める。

2.任意の年月のカレンダーを表示する
  年月を入力すると、その月のカレンダーを出力する。

3.1と2を引数の数によって場合分けし、同じファイル上に1つのプログラムとして実装する。
*/

#include <stdio.h>
#include <time.h>
#include <stdlib.h>

// 年月日を構造体で定義
typedef struct{
  int year, month, day;
} date_t;

// 今日の日付を求める関数
date_t *Getdate(date_t *today){
  time_t t;
  struct tm *lt;
  time(&t);
  lt = localtime(&t);
  today->year = lt->tm_year + 1900;
  today->month = lt->tm_mon + 1;
  today->day = lt->tm_mday;
  return today;
}

//　日数計算の関数
int Days(date_t *date){
  int y = date->year;
  int m = date->month;

  if(m <= 2){
    m += 12;
    y--;
  }

  return (365 * (y - 1926) + y / 4 - y / 100 + y / 400 + (int)(30.6 * (m + 1)) + date->day - 525);
}

int main(int argc, char *argv[]){
  date_t today, birthday, cal, next;
  int days, i;
  char *day_of_week[] = {"日","月","火","水","木","金","土"};
  Getdate(&today);

// 入力された引数の数が4の場合
if(argc == 4){
  // 誕生日を数字に変換して代入
  birthday.year = atoi(argv[1]);
  birthday.month = atoi(argv[2]);
  birthday.day = atoi(argv[3]);
	
	//　禁止入力の時の分岐
	if(birthday.year <= 1925 || birthday.year >= today.year || birthday.month >= 13 || birthday.month < 0 || birthday.day >= 32 || birthday.day < 0){
	  
	  // 禁止入力：誕生月が負または13以上、誕生日が負または32以上
	  if(birthday.month >= 13 || birthday.month < 0 || birthday.day >= 32 || birthday.day < 0){
	    printf("※入力エラー※\n");
	    printf("入力する年月日の範囲は下記のとおりです。\n");
	    printf("1925 < year < %d\n0 < month < 13\n0 < day < 32\n", today.year + 1);
	    return 0;
	  }

	  //禁止入力：1925年12月27日以前の年月日
	  if(birthday.year == 1925){
	    if(birthday.month <= 12){
	      if(birthday.day < 27){
          printf("※入力エラー※\n");
		      printf("1925年12月27日以降の年月日を入力してください。\n");
		      return 0;
        }
        else{
		      // 誕生日から現在までの日数を計算
		      days = abs(Days(&today) - Days(&birthday));
		      printf("産まれてから%d日経過しています。\n", days);
		      printf("産まれた曜日は%s曜日です。\n", day_of_week[Days(&birthday) % 7]);

          if(birthday.month == today.month && birthday.day == today.day){
		        printf("お誕生日です！\n");
          }
          return 0;
        }
      }
	  }

	  else if(birthday.year < 1925){
	    printf("※入力エラー※\n");
	    printf("1925年12月27日以降の年月日を入力してください。\n");
	    return 0;
	  }
	  
	  //禁止入力：誕生日が未来
	  else if(birthday.year >= today.year){
	    if(birthday.month >= today.month){
	      if(birthday.year > today.year || (birthday.year >= today.year && birthday.month > today.month) || birthday.day > today.day){
		      printf("※入力エラー※\n");
		      printf("入力された誕生日は未来のものです。\n");
		      return 0;
	      }
	      if(birthday.year == today.year && birthday.month == today.month && birthday.day == today.day){
          printf("今日は%s曜日です。\n", day_of_week[Days(&birthday) % 7]);
		      printf("お誕生日おめでとうございます。\n");
		      return 0;
	      }
	    }
	  }
	}
	  
  // 誕生日から現在までの日数を計算
  days = abs(Days(&today) - Days(&birthday));
  printf("産まれてから%d日経過しています。\n", days);
  printf("産まれた曜日は%s曜日です。\n", day_of_week[Days(&birthday) % 7]);
	  if(birthday.month == today.month && birthday.day == today.day){
	    printf("お誕生日です！\n");
	  }
}

// 入力された引数の数が3の場合
  else if(argc == 3){
    // 入力した任意のカレンダーの年月を数字に変換して代入
    cal.year = atoi(argv[1]);
    cal.month = atoi(argv[2]);
    cal.day = 1;

	  // 禁止入力：入力年が1925年以前
	  if(cal.year <= 1925){
	    printf("※入力エラー※\n");
	    printf("1925年以降の年を入力してください。\n");
	    return 0;
	  }

    if(cal.year < 0 || cal.month < 0){
      printf("※入力エラー※\n");
      printf("入力する年月の範囲は下記のとおりです。\n");
	    printf("1925 < year\n0 < month < 13\n");
      return 0;
    }
	
    // nextは入力した月の1ヶ月先
    next = cal;
    next.month++;
    // 入力した月の日数を求めるために1ヶ月先から差をとる
    days = Days(&next) - Days(&cal);

    printf("%d年%d月\n", cal.year, cal.month);
    //  for文を用いて曜日を記入
    for(i = 0; i < 7; i++){
      printf(" %s", day_of_week[i]);
	    // 記入終わり
      if(i == 6){
        printf("\n");
      }
    }

    // 入力した月の1日の曜日まで空白を空けていく
    for(i = 1; i <= Days(&cal) % 7; i++){
      printf("   ");
    }
      
    // 1日から順にdaysの日数だけ順に入力していく
      for(i = 1; i <= days; i++){
        cal.day++;
        printf("%3d",i);
	      // 土曜日なので改行
        if(Days(&cal) % 7 == 0){
          printf("\n");
        }
      }
      printf("\n");
  }
    
// 引数の数が違うときに、その使い方を表示する。
  else{
    printf("\n※引数エラー※\n");
    printf("誕生日から現在までの経過日数と誕生曜日を知りたい場合は <%s year month day> と入力してください。\n", argv[0]);
    printf("任意の年月のカレンダーを表示したい場合は <%s year month> と入力してください。\n\n", argv[0]);
    return 1;
  }

  return 0;
}

/* プログラムの実行結果
shii:programming isshi$ cc -Wall 17231006-cal-3.c -o ./a.out
shii:programming isshi$ ./a.out 1998 9 7
産まれてから7169日経過しています。
産まれた曜日は月曜日です。
shii:programming isshi$ ./a.out 1998 -9 7
※入力エラー※
入力する年月日の範囲は下記のとおりです。
1925 < year < 2019
0 < month < 13
0 < day < 32
shii:programming isshi$ ./a.out 1998 13 32
※入力エラー※
入力する年月日の範囲は下記のとおりです。
1925 < year < 2019
0 < month < 13
0 < day < 32
shii:programming isshi$ ./a.out 1925 9 7
※入力エラー※
1925年12月27日以降の年月日を入力してください。
shii:programming isshi$ ./a.out 1998 4 24
産まれてから7305日経過しています。
産まれた曜日は金曜日です。
お誕生日です！
shii:programming isshi$ ./a.out 2045 9 7
※入力エラー※
入力された誕生日は未来のものです。
shii:programming isshi$ ./a.out 2018 4 24
今日は火曜日です。
お誕生日おめでとうございます。
shii:programming isshi$ ./a.out 1998 9
1998年9月
 日 月 火 水 木 金 土
        1  2  3  4  5
  6  7  8  9 10 11 12
 13 14 15 16 17 18 19
 20 21 22 23 24 25 26
 27 28 29 30
shii:programming isshi$ ./a.out 1998 -9
※入力エラー※
入力する年月の範囲は下記のとおりです。
1925 < year
0 < month < 13
shii:programming isshi$ ./a.out 1925 9
※入力エラー※
1925年以降の年を入力してください。
shii:programming isshi$ ./a.out 1998 9 7 abcde

※引数エラー※
誕生日から現在までの経過日数と誕生曜日を知りたい場合は <./a.out year month day> と入力してください。
任意の年月のカレンダーを表示したい場合は <./a.out year month> と入力してください。

shii:programming isshi$
*/

/* 考察
  今回の課題では自分が産まれてからの経過日数と誕生曜日を求めるプログラム①と、任意の入力年月のカレンダーを表示するプログラム②とを組み合わせたプログラムを作成した。

  プログラム①の考察
プログラム①の実行結果を見ればわかる通り、私自身の生年月日は1998年9月7日(月曜日)であり、算出結果も月曜日と表示されており、経過日数も正しい数値のため正常に動作をしているプログラムだと言える。
しかし、このプログラム①を個人の利用だけでなく、他人にも利用できるように機能を拡張するべきだと考えた。
そのため、入力された誕生年月日が負の値であったり、月が13以上であったり、日が32以上であったり、誕生日が未来である場合などの通常ありえない数値が入力された時や
誕生年月日が1925年12月25日以前の規格外の入力がされた時に、解決方法を示したエラーを表示するような禁止入力を設定した。
実行結果を見ればわかる通り想定できうる禁止入力の適切なエラー報告ができており、プログラム①を一般向けに機能を拡張することができた。

  プログラム②の考察
プログラム②の実行結果を見ればわかる通り、今回出力したカレンダーの年月は1998年9月のものである。
プログラム①の結果から、入力年月の7日は月曜日でありプログラム②の7日も月曜日を指しているため、正常なプログラムだと言える。
また、プログラム①と同じようにプログラム②も一般向けに機能を拡張するべきだと考えた。
プログラム①同様に、入力年月に負の値が含まれる時と、入力年が1925年以前が入力された時を禁止入力として、解決方法を示したエラー表示を表示するようにした。
実行結果を見ればわかるように、想定できうる禁止入力の適切なエラー報告ができており、プログラム②を一般向けに機能を拡張することができた。
*/