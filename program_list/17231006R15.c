/*プログラミングR15 */
/* Created by 石田日々輝 on 2017/07/05 */
#include <stdio.h>
/* prmatを使って計算過程を出力 */
void prmat(double a[][8])
{
    int i,j;

    printf("\n");
    for(i=0;i<4;i++){
        for(j=0;j<8;j++){
            printf("%7.3lf  ",a[i][j]);
        }
        printf("\n");
    }
    printf("\n");
}
int main (void)
{
    double a[4][8],temp1,temp2;
    int i,j,k;
/* 行列の入力　*/
    for(i=0;i<4;i++){
        for(j=0;j<8;j++){
            if(j>3){
                if(j-4==i)
                    a[i][j]=1;
                else
                    a[i][j]=0;
            }
            else{
                printf("a[%d][%d]=",i,j);
                scanf("%lf",&a[i][j]);
            }
        }
    }
    prmat(a);
/* 逆行列を求める */
    for(k=0;k<4;k++){
        temp2=a[k][k];
            if(a[k][k]!=1){
                for(j=0;j<8;j++){
                    a[k][j]=a[k][j]/temp2;
                    }
                }
        for(i=0;i<4;i++){
            temp1=a[i][k];
            if(i!=k)
                for(j=0;j<8;j++){
                    a[i][j]=a[i][j]-a[k][j]*temp1;
            }
        }
    }
    prmat(a);
}
