# 「***」から「：」までの文字列を変更したり、行を削除してはいけない。
***レポート回数：1
***提出日：2018/10/15
***学生番号（半角数字）：17231006
***名前：石田　日々輝

***課題番号：12-2
実験目的：自分と親あるいは子のプロセス番号を表示し、正しいことを確認する。

***プログラム：
# この下に作成したプログラムのソースコードを挿入する。
/* frksmpl1.c */

#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>

int main(void){
  pid_t pid; //fork()からの返り値
  int i;
  /*　子プロセスを作る  */ 
  pid = fork();
  /* 返り値によって自分が親か子か判断する  */
  if(pid == -1){ //fork失敗
    fprintf(stderr, "fork has failed.\n");
  }
  else if(pid == 0){ //子プロセス
      printf("I am a child process\n");
      printf("My PID  = %d\n", getpid()); //自分のPID
      printf("parent PID = %d\n", getppid()); //親のPID
      sleep(2);
  }
  else{  //親プロセス
      printf("I am a parent process.\n");
      printf("My PID  = %d\n", getpid()); //自分のPID
      printf("child PID = %d\n", pid); //子のPID
      sleep(3);
  }

  return 0;
}

***実行結果：
# この下に実行コマンドとその結果を挿入する。実行コマンド行の先頭文字は「$」であること。
$ cc frksmpl1.c
$ ./a.out
I am a parent process.
My PID  = 14114
child PID = 14115
I am a child process
My PID  = 14115
parent PID = 14114

***考察：
# 実験目的が達成されていることを、実行結果を参照して理由が明確にわかるように、説明する。この下に記入する。
　I am a parent process.のときは親プロセスにいるので、My PIDは親プロセスのPID、child PIDは子プロセスのPIDである。
　実験目的を達成するためにはI am a child process.すなわち子プロセスにいるときのMy PIDが親プロセスのchild PID、
parent PIDが親プロセスのMY PIDに一致していれば良い。
上記の実行結果を参照すると、確かにPIDがそれぞれ一致していることがわかる。
従って実験目的が達成されていることがわかる。

***課題番号：12-3
実験目的：子プロセスを３つ作り、４個のプロセスの状態の変化を確認する。 

***プログラム：
# この下に作成したプログラムのソースコードを挿入する。
/* frksmpl1.c */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>

int main(){
    int pid, i, j;
    int p_id = getpid();

    /*子プロセスを3個生成*/
    for(j = 0; j < 3; j++){
        pid = fork();
    
    /* フォーク失敗 */
    if(pid == -1){
        fprintf(stderr, "fork has failed.\n");
    }
    /* 子プロセス */
    else if(pid == 0){
	    printf("I am a child process.\n");
	    printf("PPID = %d\n", getppid());
	    printf(" PID = %d\n", getpid());
	    sleep(2);
    }
    /* 親プロセス */
    else{
	    //printf("I am a parent process.\n");
	    sleep(3);
    }

    /* 子プロセスのPPIDと親プロセスのPIDが等しいときbreak */
    /* True -> その子プロセスの動作を終了 */
    if(getppid() == p_id){
        break;
    }
  }
  return 0;
}
    
***実行結果：
# この下に実行コマンドとその結果を挿入する。実行コマンド行の先頭文字は「$」であること。
$ cc report1.c
$ ./a.out
I am a child process.
PPID = 17082
 PID = 17083
I am a child process.
PPID = 17082
 PID = 17085
I am a child process.
PPID = 17082
 PID = 17087

以下、psコマンドによる動作確認
$ ps u
USER    PID  %CPU %MEM      VSZ    RSS   TT  STAT STARTED      TIME COMMAND
isshi 17083   0.0  0.0  4276944    380 s000  S+    7:23PM   0:00.00 ./a.out
isshi 17082   0.0  0.0  4267728    796 s000  S+    7:23PM   0:00.00 ./a.out
isshi 16971   0.0  0.0  4296240   1724 s001  S     7:11PM   0:00.07 -bash
isshi 13983   0.0  0.0  4288048   1172 s000  Ss    6:07PM   0:00.06 /bin/bash
shii:~ isshi$ ps u
USER    PID  %CPU %MEM      VSZ    RSS   TT  STAT STARTED      TIME COMMAND
isshi 17085   0.1  0.0  4276944    380 s000  S+    7:23PM   0:00.00 ./a.out
isshi 17082   0.1  0.0  4267728    796 s000  S+    7:23PM   0:00.00 ./a.out
isshi 16971   0.0  0.0  4296240   1724 s001  S     7:11PM   0:00.07 -bash
isshi 13983   0.0  0.0  4288048   1172 s000  Ss    6:07PM   0:00.06 /bin/bash
isshi 17083   0.0  0.0        0      0 s000  Z+    7:23PM   0:00.00 (a.out)
shii:~ isshi$ ps u
USER    PID  %CPU %MEM      VSZ    RSS   TT  STAT STARTED      TIME COMMAND
isshi 17087   0.0  0.0  4276944    380 s000  S+    7:23PM   0:00.00 ./a.out
isshi 17082   0.0  0.0  4267728    796 s000  S+    7:23PM   0:00.00 ./a.out
isshi 16971   0.0  0.0  4296240   1724 s001  S     7:11PM   0:00.07 -bash
isshi 13983   0.0  0.0  4288048   1172 s000  Ss    6:07PM   0:00.06 /bin/bash
isshi 17085   0.0  0.0        0      0 s000  Z+    7:23PM   0:00.00 (a.out)
isshi 17083   0.0  0.0        0      0 s000  Z+    7:23PM   0:00.00 (a.out)
shii:~ isshi$ ps u
USER    PID  %CPU %MEM      VSZ    RSS   TT  STAT STARTED      TIME COMMAND
isshi 16971   0.0  0.0  4296240   1724 s001  S     7:11PM   0:00.08 -bash
isshi 13983   0.0  0.0  4288048   1172 s000  Ss+   6:07PM   0:00.06 /bin/bash

***考察：
# 実験目的が達成されていることを、実行結果を参照して理由が明確にわかるように、説明する。この下に記入する。
　PPIDはそのプロセスの親プロセスのPIDで、PIDは子プロセスのPIDを表す。
　I am a child process.では子プロセスにいることを示す。
　実験結果を見て見ると、子プロセスにいるときのPPIDは全て一致していることから、親プロセスが
3つの子プロセスを作成することに成功していることがわかる。
また、子プロセスのPIDを見ると、作成した3つそれぞれのPIDが異なっているため、ここから
3つの子プロセスが区別されて作成されていることがわかる。
さらに子プロセスの作成がきちんと動作しているか確認するためにps -uコマンドを実行した。
上から順に
1. 1つ目の子プロセス作成
2. 2つ目の子プロセス作成
3. 3つ目の子プロセス作成
4. プログラム終了後
である。
ここからも子プロセスがきちんと作成され、処理が終わった順からゾンビプロセスになっている様子も確認できる。