/**********************************************************************/
/* 演習：「内部表現が極座標形式の２次元ベクトルＡＤＴ」               */
/**********************************************************************/
#include <stdio.h>
#include <math.h>               /* sqrt, atan2 使用のため */

/**********************************************************************/
/* 構造体 vector の宣言                                               */
/**********************************************************************/
typedef struct vector {
   double len;     /* 長さ(length): r */
   double ang;     /* 角度(angle):  θ */
} vec_t;

/**********************************************************************/
/* 関数のプロトタイプ宣言                                             */
/**********************************************************************/
void   vSet(double x, double y, vec_t *v);
vec_t  vCon(double x, double y);
vec_t  vAdd(vec_t u, vec_t v);
vec_t  vSub(vec_t u, vec_t v);
vec_t  vMul(double k, vec_t v);
double vProd(vec_t u, vec_t v);
void   vPrint(vec_t v);

double vGetX(vec_t v);
double vGetY(vec_t v);

/**********************************************************************/
/* メインプログラム                                                   */
/**********************************************************************/
int main(void) {
   vec_t a, b, c, d;
   double k = 7;

   vSet(5, 4, &a);  /* 方法1 */
   printf("ベクトル a = "); vPrint(a);

   b = vCon(-2, 9);  /* 方法2 */
   printf("ベクトル b = "); vPrint(b);

   printf("スカラー k = %f\n\n", k);

   printf("a + b = "); vPrint(vAdd(a, b));  /* ベクトルの和 */
   printf("a - b = "); vPrint(vSub(a, b));  /* ベクトルの差 */
   printf("k * a = "); vPrint(vMul(k, a));  /* スカラ倍 */
   printf("a . b = %f\n", vProd(a, b));     /* 内積 */
   printf("a + (1, 6) = "); vPrint(vAdd(a, vCon(1, 6)));


   printf("\n原点とベクトル a, b で作られる三角形を考える\n");
   c = vSub(a, b);
   printf("c = a - b とすると，c = "); vPrint(c);

   printf("余弦定理が成立することを計算して確かめる\n");
   printf("余弦定理： |c|^2 = |a|^2 + |b|^2 - 2 * a.b\n");
   printf("   ここで a.b は「a と b の内積」のこと\n\n");

   printf("|c|^2    = %f\n", vProd(c, c));
   printf("|a|^2    = %f\n", vProd(a, a));
   printf("|b|^2    = %f\n", vProd(b, b));
   printf("2*a.b    = %f\n", 2*vProd(a, b));
   printf("右辺の値 = %f\n\n", vProd(a, a) + vProd(b, b) - 2*vProd(a, b));

   /* 「a から b を引いたベクトル」に「a と b の内積」を掛けてできた  */
   /* ベクトルに (9, 6) と成分表示されるベクトルを加えたものを        */
   /* d に代入する．以下の ??????? の部分を埋めて処理を完成させよ     */

   d = vAdd(vMul(vProd(a, b), vSub(a, b)), vCon(9, 6));
   printf("d = (a . b) * (a - b) + (9, 6) = "); vPrint(d);

   return 0;
}

/**********************************************************************/
/* ベクトル操作関数の定義                                             */
/**********************************************************************/
/* ベクトルの x 成分を返す */
double vGetX(vec_t v) {
   return v.len * cos(v.ang);
}

/* ベクトルの y 成分を返す */
double vGetY(vec_t v) {
    return v.len * sin(v.ang);
   /* 上と同様に */
   /* return ... */
}

/* ベクトルの値の設定：(方法1) 副作用による方法 */
void vSet(double x, double y, vec_t* v) {
    v->len = sqrt(x * x + y * y);
    v->ang = atan2(y / x);
 /*
   x と y から len と ang を計算して設定する

   v->len = ...
   v->ang = ...

 */
}

/* ベクトルの値の設定：(方法2) 戻り値による方法 */
vec_t vCon(double x, double y) {
   vec_t res;

   /* vSet が (x, y) 形式 から (r, θ) 形式への */
   /* 変換を行なってくれるので，変更の必要なし! */

   vSet(x, y, &res);

   return res;
}

/* ベクトルの和 : u + v */
vec_t vAdd(vec_t u, vec_t v) {
    return vCon((vGetX(u) + vGetX(v)),(vGetY(u) + vGetY(v)));

 /*
   u の x 成分 : vGetX(u)
   u の y 成分 : vGetY(u)
   v の x 成分 : vGetX(v)
   v の y 成分 : vGetY(v)

   で取得できることをふまえて，
      「u の x 成分 と v の x 成分の和」と
      「u の y 成分 と v の y 成分の和」を
   引き数に与えて vCon を呼び出してベクトルを作成して返す
 */
}

/* 以下,関数を作成する */
