//
// ex03.cc - [演習3] プログラムファイル
//	作者: 石田　日々輝; 日付: 2019/01/21
// [注意] 以降の演習では，上記を適切に書き換えること
//

#include "rt03.h"
#include <iostream>
using namespace std;

//
// RTHit::intensity() - ピクセルの輝度(0〜255)を返す
//
unsigned char
RTHit::intensity(const RTLSrc& ls, double ia)
{
  double cos = nor.dotprod(ls.g_dir());
  double kaIa = obj->g_ka() * ia;
  double cosa = ref.vec.dotprod(ls.g_dir());
  double kaIa_KdIlcos  = kaIa + obj->g_kd() * ls.g_it() * nor.dotprod(ls.g_dir());
  double kdIlcos = obj->g_kd() * ls.g_it() * nor.dotprod(ls.g_dir());
  double I, ksIlcosna, cosna;
  if(cos < 0){
	if(kaIa > 255){
		return 255;
	}
	else{
		return kaIa;
	}
  }
  
  else if(cosa < 0){
	if(kaIa_KdIlcos > 255){
		return 255;
	}
	else{
		return kaIa_KdIlcos;
	}
  }
  else{
	cosna = 1;
  	for(int i = 0; i < obj->g_n(); i++){
		cosna = cosna * cosa;
 	 } 
  	double I = obj->g_ka() * ia + 
	     	   obj->g_kd() * ls.g_it() * nor.dotprod(ls.g_dir()) + 
	     	   obj->g_ks() * ls.g_it() * cosna;
  	if(I > 255){
		return 255;
	}
	else{
		return I;
	}
  }
}

//
// main() - [演習3] メインルーチン
//
int
main()
{
        RTPoint vp, vrp, cnt;
	RTVec dir;
	double angle, rad, kd, it, ka, ia, ks;
	int ds, n;

// ビューの設定 (標準入力からデータ入力)
	cin >> vp.c[0] >> vp.c[1] >> vp.c[2];	// 視点の入力
	cin >> vrp.c[0] >> vrp.c[1] >> vrp.c[2]; // 参照視点の入力
	cin >> angle >> ds;	// 視野角とピクセル数(各半分)の入力
	RTViewMgr view(vp, vrp, angle, ds);
// 物体(球)データの設定 (標準入力からデータ入力)
	cin >> cnt.c[0] >> cnt.c[1] >> cnt.c[2]; // 球の中心の入力
	cin >> rad;                              // 球の半径の入力
	cin >> kd;                               // 拡散反射率の入力
	cin >> ka;				 // 環境反射率の入力
        cin >> ks;				 // 鏡面反射率の入力
	cin >> n;				 // 鏡面反射べき指数の入力
	RTObject obj(cnt, rad, kd, ka, ks, n);
//拡散反射率の設定
	cin >> dir.c[0] >> dir.c[1] >> dir.c[2];
	cin >> it;
	RTLSrc line(dir, it);
// 環境光の強さの入力
	cin >> ia;
//
// 各ピクセルの輝度の計算と標準出力への出力
	view.prnpgmhdr();	// PGMファイルのヘッダ出力
	RTRay ray;
	RTHit hit;
	for(int y = 2 * ds - 1; y >= 0; y--)
		for(int x = 0; x < 2 * ds; x++) {
			view.getray(x, y, &ray); // ピクセルへの光線を得る
			if(obj.rayhit(ray, &hit))
				cout << hit.intensity(line, ia);
			else
				cout << static_cast<unsigned char>(0);
		}
}
