#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main(void)
{
    int i,j,n,x,y,z,temp,a[5][5],b[100];

    x=1;
    y=15;
    z=1;
// 配列b[i]の初期値を１にする. //
    for(i=0;i<100;i++)
        b[i]=1;
// 25個の整数乱数を配列a[i][j]に代入していく. //
    srand(time(NULL));
    for(j=0;j<5;j++){
        for(i=0;i<5;i++){
            a[i][j]=rand() %y+1;
// 行15刻みに乱数を発生させるための条件 //
            if(b[a[i][j]]==1 && a[i][j]>=z && a[i][j]<=y){
                b[a[i][j]]=0;
            }
            else
                i--;
        }
        y=y+15;
        z=z+15;
    }
// 上の状態では行刻みになっているので、行と列の入れ替えを行う. //
    for(i=0;i<5;i++)
        for(j=0;j<5;j++){
            temp=a[i][j];
            a[i][j]=a[j][i];
            a[j][i]=temp;
        }
    for(i=0;i<5;i++){
        for(j=0;j<5;j++)
            printf("%2d ",a[i][j]);
        printf("\n");
    }
// 任意の数字が配列内にあるかのチェック. //
    printf("1から75までの数字を入力してください. ");
    scanf(" %d",&n);
    for(i=0;i<5;i++)
        for(j=0;j<5;j++){
            if(a[i][j]==n){
                printf("%dは(%d,%d)成分です.\n",n,i+1,j+1);
                x=0;
                break;
            }
        }
    if(x==1)
        printf("%dはこの行列の成分ではありません.\n",n);
    return 0;
}