# -*- coding: utf-8 -*-

from flask import Flask, render_template, request, redirect, url_for
import numpy as np
import datetime
import collections

Rarities = ["'★'", "'★★'", "'★★★'"]
SPRarities = ["'★★'", "'★★★'"]
AllResult = []
worst = 0

app = Flask(__name__)

# Main
class VO(object):
    def __init__(self):
        self._count = 0
        self._price = 0

    def getcount(self):
        return self._count

    def setcount(self, count):
        self._count = count

    def getprice(self):
        return self._price

    def setprice(self, price):
        self._price = price

    count = property(getcount, setcount)
    price = property(getprice, setprice)

vo = VO()

def PlaySingleGacha() :
    SingleResult = [] #シングルガチャの結果を格納
    GachaTimes = len(SingleResult)
    SingleResult.append(GachaSystem(GachaTimes))
    return SingleResult

def PlayTenTimesGacha() :
    TenTimesResult = [] #10連ガチャの結果を格納
    for i in range(10) :
        GachaTimes = len(TenTimesResult)
        TenTimesResult.append(GachaSystem(GachaTimes))
    return TenTimesResult

def GachaSystem(GachaTimes) :
    if(GachaTimes == 9) :
        PickUp = np.random.choice(SPRarities, p = [0.98, 0.02])
    else :
        PickUp = np.random.choice(Rarities, p = [0.80, 0.18, 0.02])
    AllResult.append(PickUp)
    return PickUp

def resAllResult() :
    return collections.Counter(AllResult)

# Routing
@app.route('/')
def index():
    title = "ようこそ"
    message = "ガチャを回すにはボタンをクリックしてください"
    return render_template('index4.html',
                           message=message, title=title)

@app.route('/post', methods=['POST', 'GET'])
def post():
    global AllResult
    time = datetime.datetime.today().strftime("%H:%M:%S")
    message = ""
    if request.method == 'POST':
        result = []
        resultall = []
        if 'rare' in request.form:
            title = "ガチャを回しました！"
            vo.price = vo.price + 300
            vo.count = vo.count + 1
            result = PlaySingleGacha()
        if '10rare' in request.form:
            title = "ガチャを回しました！"
            vo.price = vo.price + 3000
            vo.count = vo.count + 10
            result = PlayTenTimesGacha()
        if 'reset' in request.form:
            title = "リセットしました"
            vo.price = 0
            vo.count = 0
            result = ""
            resultall = ""
            AllResult = []
            message = "リセットしました"
        if 'all' in request.form:
            print(collections.Counter(AllResult))
            title = "ガチャ結果一覧です"
            vo.price = vo.price
            vo.count = vo.count
            result = ""
            resultall = resAllResult()
        return render_template('index4.html',
                               result=result, title=title,
                               time=time, vo=vo,
                               message=message, resultall=resultall)
    else:
        return redirect(url_for('index'))

if __name__ == '__main__':
    app.debug = True
    app.run(host='0.0.0.0')