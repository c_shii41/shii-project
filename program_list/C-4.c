#include <WProgram.h>

#define ledPin 4
#define DPin 6

//6番を出力、4番を入力に設定
void setup(){
  pinMode(DPin, OUTPUT);
  pinMode(ledPin, INPUT);
}

//5Vのアナログ値は1023だから4で割って255付近の値にする
void loop(){
  value = analogRead(ledPin);
  analogWrite(DPin, value / 4);
    
}

int main(void){
  int value;
  
  init();

  setup();
  
  //無限ループ
  while(1){
      loop();
  }
  return 0;
}
