import socket
import threading
from sys import exit

# Socket通信初期化
host = '0.0.0.0'
port = 60103
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((host, port))
s.listen(10)

client_list = []  # 接続中のクライアント情報を格納するリスト
all_data = []  # 通信情報を格納するリスト


# 各種コマンド
def command(connection, data):
    # 過去ログを送信
    if data == 'Log':
        data = data.replace('Log ', '')
        for i, data in enumerate(all_data):
            past_data = '{0} {1}\n'.format(i, data)
            connection.send(past_data.encode('UTF-8'))
    # 参加中のユーザー名一覧を送信
    elif data == 'List':
        for user_name in client_list:
            send_string = user_name[2] + '\n'
            connection.send(send_string.encode('UTF-8'))
    # return値はコマンドだったかどうか
    else:
        return False
    return True


# 各クライアントとの送受信のコア部分
def handler(connection, address, user_name):
    continue_flag = True
    while True:
        if not continue_flag:
            return
        try:
            res = connection.recv(4096)
            send_string = '[{0}] {1}'.format(str(user_name), res.decode())
            # 空データの場合の終了処理
            if not res or res.decode() == 'eNdCoNnEcTiOn':
                connection.send('eNdCoNnEcTiOn'.encode('UTF-8'))
                send_string = '[{0}] ** 退室しました **'.format(str(user_name))
                connection.close()
                client_list.remove((connection, address, user_name))
                print('[接続終了] アドレス：{0}'.format(address[0]))
                print('[接続終了] ポート　：{0}'.format(address[1]))
                continue_flag = False

            # コマンドだったら次ループへ突入
            elif command(connection, res.decode()):
                continue

            all_data.append(send_string)
            # 接続中のクライアント情報すべてにアクション実行
            for eachClient in client_list:
                clientAdress = eachClient[1][0]
                clientPort = eachClient[1][1]
                receivedAdress = address[0]
                receivedPort = address[1]
                # 送信元と同じクライアントの場合はコンソール出力
                if clientAdress == receivedAdress \
                   and clientPort == receivedPort:
                    print('[{0}:{1}] {2}'.format(
                        receivedAdress,
                        receivedPort,
                        res.decode()
                        ))
                # 違う場合は送信
                else:
                    eachClient[0].send(send_string.encode('UTF-8'))
        except Exception as e:
            # 例外発生時の処理
            print(e)
            return


# ユーザー名の取得
def client_setting(connection, address):
    # ユーザー名の送信を促す
    while True:
        # 重複しているかどうかのフラグ
        connection.send('任意のユーザー名を送信してください．\n'.encode('UTF-8'))
        res = connection.recv(4096)
        user_name = res.decode()

        # 接続中のクライアントから同じユーザー名が無いか確認
        for check in client_list:
            if (check[2] == user_name):
                connection.send('E101'.encode('UTF-8'))
                break
        # 重複がない場合
        else:
            connection.send('I200'.encode('UTF-8'))
            break

    print('[新規接続] アドレス　：{0}'.format(address[0]))
    print('[新規接続] ポート　　：{0}'.format(address[1]))
    print('[新規接続] ユーザー名：{0}'.format(user_name))

    # ログに記録してほかユーザーに送信
    send_string = '[{0}] ** 入室しました **'.format(user_name)
    all_data.append(send_string)
    for eachClient in client_list:
        eachClient[0].send(send_string.encode('UTF-8'))
    # クライアント情報を追加
    client_list.append((connection, address, user_name))
    connection.send('ようこそ，{0}さん\n'.format(user_name).encode('UTF-8'))
    # 今までのログを送信
    for i, data in enumerate(all_data):
        past_data = '{0} {1}\n'.format(i, data)
        connection.send(past_data.encode('UTF-8'))

    # この接続用のコントローラを並列処理で実行
    thread_handler = threading.Thread(
        target=handler,
        args=(connection, address, user_name)
        )
    thread_handler.start()

    return


if __name__ == "__main__":
    while True:
        try:
            # クライアントからの接続要求受け入れ・確立
            connection, address = s.accept()

            # 今接続したクライアントの設定とハンドラの開始
            thread_setting = threading.Thread(
                target=client_setting,
                args=(connection, address)
                )
            thread_setting.start()

        except Exception as e:
            print(e)
            break
        except KeyboardInterrupt:
            print(' Interrupted')
            exit(0)
