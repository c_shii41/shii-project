#include <stdio.h>
void prmat(double a[][6])
{
    int i,j;

    printf("\n");
    for(i=0;i<6;i++){
        for(j=0;j<6;j++){
            printf("%7.3lf  ",a[i][j]);
        }
        printf("\n");
    }
    printf("\n");
}
int main (void)
{
    double a[3][6],temp1,temp2;
    int i,j,k;
/* 行列の入力　*/
    for(i=0;i<3;i++){
        for(j=0;j<3;j++){
            if(j>2){
                if(j-3==i)
                    a[i][j]=1;
                else
                    a[i][j]=0;
            }
            else{
                printf("a[%d][%d]=",i,j);
                scanf("%lf",&a[i][j]);
            }
        }
    }
    /* 上で入力した行列を出力 */
    prmat(a);

    /* (i,i)成分を１にする */
    /* (i,0)成分を１にする */
    /* (第2,3,4行)-(第1行)をする。 */
    for(i=0;i<3;i++){
        if(a[i][i]!=1){
            temp1=a[i][i];
            for(j=0;j<6;j++){
                a[i][j]=a[i][j]/temp1;
            }
        }
    }
    prmat(a);
    for(i=0;i<3;i++){
        if(a[i][0]!=1){
            temp1=a[i][0];
            a[i][0]=1;
            temp2=a[i][0]/temp1;
            for(j=1;j<6;j++){
                a[i][j]=a[i][j]*temp2;
            }
        }
    }
    prmat(a);
    for(i=1;i<3;i++){
        for(j=0;j<6;j++){
            a[i][j]=a[i][j]-a[0][j];
        }
    }
    prmat(a);
    /* (i,i)成分を１にする */
    /* (i,1)成分を１にする */
    /* (第1,3,4行)-(第2行)をする。 */
    for(i=0;i<3;i++){
        if(a[i][i]!=1){
            temp1=a[i][i];
            for(j=0;j<6;j++){
                a[i][j]=a[i][j]/temp1;
            }
        }
    }
    prmat(a);
    for(i=0;i<3;i++){
        if(a[i][1]!=1){
            temp1=a[i][1];
            temp2=1/temp1;
            for(j=0;j<6;j++){
                a[i][j]=a[i][j]*temp2;
            }
        }
    }
    prmat(a);
    for(i=0;i<3;i++){
        for(j=0;j<6;j++){
            if(i!=1)
                a[i][j]=a[i][j]-a[1][j];
            }
    }
    prmat(a);
    /* (i,i)成分を１にする */
    /* (i,2)成分を１にする */
    /* (第1,2,4行)-(第3行)をする。 */
    for(i=0;i<3;i++){
        if(a[i][i]!=1){
            temp1=a[i][i];
            for(j=0;j<6;j++){
                a[i][j]=a[i][j]/temp1;
            }
        }
    }
    prmat(a);
    for(i=0;i<3;i++){
        if(a[i][2]!=1){
            temp1=a[i][2];
            temp2=1/temp1;
            for(j=0;j<6;j++){
                a[i][j]=a[i][j]*temp2;
            }
        }
    }
    prmat(a);
    for(i=0;i<3;i++){
        for(j=0;j<6;j++){
            if(i!=2)
                a[i][j]=a[i][j]-a[2][j];
            }
    }
    prmat(a);
    /* (i,i)成分を１にする */
    /* (i,3)成分を１にする */
    /* (第1,2,3行)-(第4行)をする。 */
    for(i=0;i<3;i++){
        if(a[i][i]!=1){
            temp1=a[i][i];
            for(j=0;j<6;j++){
                a[i][j]=a[i][j]/temp1;
            }
        }
    }
    prmat(a);
    for(i=0;i<3;i++){
        if(a[i][3]!=1){
            temp1=a[i][3];
            temp2=1/temp1;
            for(j=0;j<6;j++){
                a[i][j]=a[i][j]*temp2;
            }
        }
    }
    prmat(a);
    for(i=0;i<3;i++){
        for(j=0;j<6;j++){
            if(i!=3)
                a[i][j]=a[i][j]-a[3][j];
            }
    }
    prmat(a);
    for(i=0;i<3;i++){
        if(a[i][i]!=1){
            temp1=a[i][i];
            for(j=0;j<6;j++){
                a[i][j]=a[i][j]/temp1;
            }
        }
    }
    prmat(a);

    return 0;
}