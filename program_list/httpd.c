#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>

#define PORT 19992

main()
{
  int i, s, t, len, x, y, count = 0;
  struct sockaddr_in saddr, caddr;
  char   buf[100];

  saddr.sin_family = AF_INET;
  saddr.sin_addr.s_addr = INADDR_ANY;
  saddr.sin_port = htons(PORT);

  s = socket(AF_INET, SOCK_STREAM, 0);
  bind(s, (struct sockaddr *)&saddr, sizeof(struct sockaddr_in));
  listen(s,10);

  for (;;) {
    len = sizeof(caddr);
    t = accept(s, (struct sockaddr *)&caddr, &len);
    read(t, buf, 100);
    sprintf(buf, "HTTP/1.1 200 OK\r\n" 
	    "text/html\r\n\r\n"
	    "<body>Hello, World (count=%d)<br></body>\r\n" 
	    "</html>\r\n", count++);
    write(t, buf, strlen(buf));
    close(t);
  }
}
