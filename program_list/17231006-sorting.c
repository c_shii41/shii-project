/*
[課題]
高速なソート法の前に、素朴なソート法について、実行時間の計測をするプログラムを完成させ
レポートの最低条件として以下を満たしていることを確認してから提出すること．
ただし，締切に間に合わない場合は，提出は不要である．
提出したレポートについては，採点者によるチェックを行うので，できるだけ提出すること．

１. ソートの時間とデータ数についての両対数グラフ（紙のレポートのみ）があること．
・バブルソート
・セレクションソート
・インサーションソート
ただし、データの個数 10^4 〜 10^5 程度のデータで確認していること

２．グラフとオーダーの関係について考察があること

３. ソートの速度の違いについて考察があること．
*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

//#define MAXLEN 1000 //配列の大きさ
#define max_size 5000
#define max_element 10000
typedef int element_type; //データの型


void rand_create(int data[], int i){
  int j;

  for(j = 0; j < i; j++){
    data[j] = random() % max_element;
  }
}

void print_Array(int x[], int n){
  int i;

  for(i = 0; i < n; i++){
    printf("%d ",x[i]);
  }
  printf("\n");
}

//バブル法
void bubble_sort(element_type data[], int n){
  int i, j, temp;
  
  for(i = 0; i < n - 1; i++){
    for(j = n - 1; j > i; j--){
      if(data[j - 1] > data[j]){
	      temp = data[j - 1];
	      data[j - 1] = data[j];
	      data[j] = temp;
      }
    }
  }
}

//選択法
void selection_sort(element_type data[], int n){
  int i, j, temp;
  
  for(i = 0; i < n - 1; i++){
    for(j = i + 1; j < n; j++){
      if(data[i] > data[j]){
	      temp = data[i];
	      data[i] = data[j];
	      data[j] = temp;
      }
    }
  }
}

//挿入法
void insertion_sort(element_type data[], int n){
  int i, j;
  element_type temp;
  
  for(i = 1; i < n; i++){
    temp = data[i];
    for(j = i; j > 0 && data[j - 1] > temp; j--){
      data[j] = data[j - 1];
    }
    data[j] = temp;
  }
}

int main(void)
{
    clock_t start_time, end_time;
    double cpu_time;

    
    printf(" 要素数   バブルソート   選択ソート   挿入ソート \n");
    int i = max_size;
    for (;;) {
        int data[i];
        srandom(time(NULL));

        printf("%7d   ", i);
        printf(" 処理中 ");
        fflush( stdout );
                
        start_time = clock();
        rand_create(data, i);
        bubble_sort(data, i);
        //print_Array(data, i);
        end_time = clock();
        cpu_time = (double)(end_time - start_time) / CLOCKS_PER_SEC;
        printf(" \b\b\b\b\b\b\b\b\b\b\b\b%10.5f s     ", cpu_time);
        printf("  処理中 "); 
        fflush( stdout );
        
        start_time = clock();
        rand_create(data, i);
        selection_sort(data, i);
        //print_Array(data, i);
        end_time = clock();
        cpu_time = (double)(end_time - start_time) / CLOCKS_PER_SEC;
        printf("\b\b\b\b\b\b\b\b\b\b\b%10.5f s    ", cpu_time);
        printf(" 処理中 "); 
        fflush( stdout );

        start_time = clock();
        rand_create(data, i);
        insertion_sort(data, i);
        //print_Array(data, i);
        end_time = clock();
        cpu_time = (double)(end_time - start_time) / CLOCKS_PER_SEC;
        printf("\b\b\b\b\b\b\b\b\b\b\b%10.5f s\n", cpu_time);
        fflush( stdout );
      
        i = i + 5000;
        if(i > 200000){
          return 0;
      }
    }
} 

/*
実行結果
shii:programming isshi$ cc -Wall 17231006-sorting.c
shii:programming isshi$ ./a.out
 要素数   バブルソート   選択ソート   挿入ソート
   5000   0.06366 s      0.07791 s    0.02194 s
  10000   0.25789 s      0.30878 s    0.07388 s
  15000   0.61353 s      0.66151 s    0.17018 s
  20000   1.15196 s      1.13629 s    0.32892 s
  25000   1.79186 s      1.65027 s    0.44429 s
  30000   2.55194 s      2.27545 s    0.64107 s
  35000   3.50402 s      2.96486 s    0.86760 s
  40000   4.61805 s      3.69759 s    1.12936 s
  45000   5.87329 s      4.52159 s    1.43995 s
  50000   7.29815 s      5.38931 s    1.76414 s
  55000   8.87509 s      6.33783 s    2.16563 s
  60000  10.62641 s      7.31991 s    2.54820 s
  65000  12.52829 s      8.33467 s    2.97093 s
  70000  14.62720 s      9.47073 s    3.47544 s
  75000  16.76662 s     10.63157 s    3.99477 s
  80000  19.12382 s     11.86813 s    4.52875 s
  85000  21.60802 s     13.14898 s    5.12087 s
  90000  24.32183 s     14.47655 s    6.03969 s
  95000  27.71617 s     16.16611 s    6.51135 s
 100000  30.31123 s     17.91813 s    7.36801 s
 105000  33.43636 s     18.93030 s    7.82843 s
 110000  36.89669 s     20.97028 s    8.63363 s
 115000  40.22512 s     22.13424 s    9.44302 s
 120000  43.77933 s     24.11016 s   10.31167 s
 125000  47.44646 s     25.54806 s   11.07392 s
 130000  51.26400 s     27.34345 s   11.96161 s
 135000  55.49499 s     29.17219 s   12.88581 s
 140000  59.50833 s     31.07830 s   13.91057 s
 145000  63.84233 s     33.08028 s   14.91951 s
 150000  68.37306 s     35.15165 s   15.93493 s
 155000  75.18287 s     38.92900 s   17.17434 s
 160000  81.96673 s     41.12065 s   19.41869 s
 165000  85.10301 s     42.79109 s   20.09355 s
 170000  91.47720 s     45.81114 s   21.24619 s
 175000  97.37720 s     47.82127 s   22.79262 s
 180000 102.69788 s     50.46818 s   23.34445 s
 185000 108.93280 s     52.86826 s   25.73520 s
 190000 114.04482 s     55.82010 s   26.70174 s
 195000 121.77005 s     58.25794 s   28.25745 s
 200000 128.69024 s     61.50249 s   30.74848 s
*/

/*
グラフとオーダーの関係についての考察
  両対数グラフに、縦軸に要素数、横軸に時間としてプロットしたところ直線比例グラフとなった。
この理由としてバブル法、選択法、挿入法のいずれも、すべて全体ではO(n)回同様の処理を繰り返し、
1回にO(n)の時間がかかっているが為に、計算時間のオーダーがO(n^2)となるためである。
*/

/*
ソートの速度の違いについての考察
・バブルソート
  比較回数:n(n-1)/2回
  交換回数(平均):n/2回
  交換回数(全体):n(n-1)/4回
・選択法
  比較回数:n(n-1)/2回
  交換回数:n-1回
・挿入法
  比較回数:n(n-1)/2以下
  交換回数:バブルソートと同等

●バブルソートと選択法のソート速度比較
比較回数が同じなので、交換回数で差が生じる。
交換回数の差を取ってみると
n(n-1)/4-(n-1)=(n-1)(n/4-1)となる。
この二次関数は1<n<4の範囲で負になるが、これはバブルソートの大部分において
選択法よりも交換回数が多く、さらに時間差は平方数で伸びていくことを意味する。
実際、実行結果において、要素数が増えて交換回数が増えていくに連れ選択法の方が
ソートが早いことが見てわかる。

●選択法と挿入法のソート速度比較
上記で示したバブルソートよりも選択法の方がソート時間が速いことを前提とする。
選択法と挿入法では比較回数が挿入法の方が少ない。よってソート時間が比較回数においては
挿入法の方が速い。
次に交換回数は挿入法はバブルソートと同等なので、前提からソート時間は交換回数の面から見ても
挿入法の方が速いとわかる。

●結論
以上の比較からソートの速度は
バブルソート < 選択法 < 挿入法 となる。
*/

/*
全体の考察
主にfor文を用いてソートのプログラムを作成した。
ソートの種類は３種類で、バブルソート、選択法、挿入法である。
擬似乱数の発生にはライブラリ関数であるrand()、srand()があるが乱数の質が
あまり良くないので、random()、srandom()を用いて乱数を発生させた。
システム時間の計測には<time.h>のclock()を用いた。
要素数が少ないときは被りが少ないのでソート時間が安定しない。
グラフを見ればわかるように要素数が少ない間はグラフがややギザギザになっていることがわかる。
要素数が増えるごとに直線比例グラフになり、上から順にバブルソート、選択法、挿入法
の順にソートが速いことがわかり、ソートの速度比較を理論とグラフの両方で示すことができた。
*/

