#include <stdio.h>
void prmat(double a[][8])
{
    int i,j;

    printf("\n");
    for(i=0;i<4;i++){
        for(j=0;j<8;j++){
            printf("%7.3lf  ",a[i][j]);
        }
        printf("\n");
    }
    printf("\n");
}
int main (void)
{
    double a[4][8],temp1,temp2;
    int i,j,k,l;
/* 行列の入力　*/
    for(i=0;i<4;i++){
        for(j=0;j<8;j++){
            if(j>3){
                if(j-4==i)
                    a[i][j]=1;
                else
                    a[i][j]=0;
            }
            else{
                printf("a[%d][%d]=",i,j);
                scanf("%lf",&a[i][j]);
            }
        }
    }
    prmat(a);
    /* (i,i)成分を１にする */
    /* (第2,3,4行)-(第1行)をする。 */
    for(i=0;i<4;i++){
        if(a[i][i]!=1){
            temp1=a[i][i];
            for(j=0;j<8;j++){
                a[i][j]=a[i][j]/temp1;
            }
        }
    }
    prmat(a);
    for(i=0;i<4;i++){
        temp1=a[i][0];
        if(i!=0)
            for(j=0;j<8;j++){
                a[i][j]=a[i][j]-a[0][j]*temp1;
        }
    }
    prmat(a);
    /* (i,i)成分を１にする */
    /* (i,1)成分を１にする */
    /* (第1,3,4行)-(第2行)をする。 */
    for(i=0;i<4;i++){
        if(a[i][i]!=1){
            temp1=a[i][i];
            for(j=0;j<8;j++){
                a[i][j]=a[i][j]/temp1;
            }
        }
    }
    prmat(a);
    for(i=0;i<4;i++){
        temp1=a[i][1];
        if(i!=1)
            for(j=0;j<8;j++){
                a[i][j]=a[i][j]-a[1][j]*temp1;
        }
    }
    prmat(a);
    /* (i,i)成分を１にする */
    /* (i,2)成分を１にする */
    /* (第1,2,4行)-(第3行)をする。 */
    for(i=0;i<4;i++){
        if(a[i][i]!=1){
            temp1=a[i][i];
            for(j=0;j<8;j++){
                a[i][j]=a[i][j]/temp1;
            }
        }
    }
    prmat(a);
    for(i=0;i<4;i++){
        temp1=a[i][2];
        if(i!=2)
            for(j=0;j<8;j++){
                a[i][j]=a[i][j]-a[2][j]*temp1;
        }
    }
    prmat(a);
    /* (i,i)成分を１にする */
    /* (i,3)成分を１にする */
    /* (第1,2,3行)-(第4行)をする。 */
    for(i=0;i<4;i++){
        if(a[i][i]!=1){
            temp1=a[i][i];
            for(j=0;j<8;j++){
                a[i][j]=a[i][j]/temp1;
            }
        }
    }
    prmat(a);
    for(i=0;i<4;i++){
        temp1=a[i][3];
        if(i!=3)
            for(j=0;j<8;j++){
                a[i][j]=a[i][j]-a[3][j]*temp1;
        }
    }
    prmat(a);

    return 0;
}