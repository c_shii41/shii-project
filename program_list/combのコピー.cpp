#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <time.h>
#include <iostream>

#define Array_size 100
int r_count = 0;
int d_count = 0;

unsigned long long int r_comb(int n, int m){
  r_count++;
  if(m == 0 || m == n){
    return 1;
  }
  else{
    return r_comb(n - 1, m) + r_comb(n - 1,m - 1);
  }
}

unsigned long long int d_comb(int n, int m){

  if((n - m) < n / 2){
    m = n - m;
  }

  std::cout << n << m;

  unsigned long long int dp[n - m + 1][m + 1];
  int i, j;
 
  /*for(i = 0; i <= n - m; i++){
    dp[i][0] = 1;
    d_count++;
  }
  for(i = 1; i <= m; i++){
    dp[0][i] = 1;
    d_count++;
  }*/

  for(i = 0; i <= n - m; i++){
    for(j = 0; j <= m; j++){
      if(i == 0 || j == 0){
        dp[i][j] = 1;
      }
      else{
        dp[i][j] = dp[i - 1][j] + dp[i][j - 1];
      }
      //printf("%d,%d\n", i, j);
      d_count++;
    }
  }

  for(i = 0; i <= n - m; i++){
    for(j = 0; j <= m; j++){
      printf("%11llu", dp[i][j]);
      if(j == m){
        printf("\n");
      }
    }
  }
  return dp[n - m][m];
}

int num1(char value){
  return atoi(&value);
}

int number(char *expr, int *next){
  int value;
  
  if(!isdigit(expr[*next])){;
    exit(1);
  }
  else{
    value = num1(expr[*next]);
    (*next)++;
    while(isdigit(expr[*next])){
      value = value * 10 + num1(expr[*next]);
      (*next)++;
    }
  }
  (*next) = 0;
  return value;
}

int main(void){
  int n, m, pos = 0;
  unsigned long long int ans, answer;
  char value[Array_size];
  clock_t start_time, end_time;
  double cpu_time;

  while(1){
    int con = 0;
    r_count = 0;
    d_count = 0;
    printf("n C m\n");
  
    printf("n = ");
    std::cin >> n;
    n = number(value, &pos);

    printf("m = ");
    std::cin >> m;
    m = number(value, &pos);
    //printf("%d C %d", n, m);
    
    if(n < m){
      printf("Error : n < m となる値は存在しません。\n");
      continue;
    }

    start_time = clock();
    answer = d_comb(n, m);
    end_time = clock();
    cpu_time = (double)(end_time - start_time) / CLOCKS_PER_SEC;
    printf("\n%llu\n", answer);
    printf("%8lf sec\n", cpu_time);
    printf("実行回数は%d回\n", d_count);
    
    start_time = clock();
    ans = r_comb(n, m);
    end_time = clock();
    cpu_time = (double)(end_time - start_time) / CLOCKS_PER_SEC;
    printf("%llu\n", ans);
    printf("%10lf sec\n", cpu_time);
    printf("実行回数は%d回\n", r_count);

    printf("Continue  1 / 0\n");
    
    fgets(value, Array_size, stdin);
    con = number(value, &pos);
    
    if(con == 1){
      continue;
    }
    else{
      printf("プログラムを終了します。\n");
      return 0;
    }
  }
  return 0;
}
