//
// ex04.cc - [演習4] プログラムファイル
//	作者: 石田　日々輝; 日付: 2019/01/28
// [注意] 以降の演習では，上記を適切に書き換えること
//

#include "rt04.h"
#include <iostream>
using namespace std;
//
// RTHit::intensity() - ピクセルの輝度(0〜255)を返す
//
unsigned char
RTHit::intensity(const list<RTLSrc*>& lslist, double ia){
	int i;
        double t = 0;
	double cos, num1, num2, cosa, cosna;
  	for(RTLSrc* ls: lslist){
		cos = nor.dotprod(ls->g_dir());
		num1 = obj->g_ka() * ia;
 	 	if(cos < 0){
			if(num1 > 255){
				num2 = 255;
			}
			else{
				num2 = obj->g_ka() * ia;
			}
	  	}
  
 		else {
			cosa = ref.vec.dotprod(ls->g_dir());
			num2 = num1 + obj->g_kd() * ls->g_it() * nor.dotprod(ls->g_dir()); 
			if(cosa < 0){
				if(num2 > 255){
					num2 = 255;
				}
			}	

  			else{
				cosna = 1;
  				for(int i = 0; i < obj->g_n(); i++){
					cosna = cosna * cosa;
 				} 
 	 			num2 = num2 + obj->g_ks() * ls->g_it() * cosna;
  				if(num2 > 255){
					num2 = 255;
				}
			}	
  		}
		t = t + num2;
	}	
	if(t > 255){
		return 255;
	}
	else{
		return t;
	}
}

//
// main() - [演習4] メインルーチン
//
int
main()
{
        RTPoint vp, vrp, cnt;
	RTVec dir;
	double angle, rad, kd, it, ka, ia, ks;
	int ds, n, i, obj_num, dir_num;
	list<RTLSrc*> line;
	list<RTObject*> obj;

// ビューの設定 (標準入力からデータ入力)
	cin >> vp.c[0] >> vp.c[1] >> vp.c[2];	// 視点の入力
	cin >> vrp.c[0] >> vrp.c[1] >> vrp.c[2]; // 参照視点の入力
	cin >> angle >> ds;	// 視野角とピクセル数(各半分)の入力
	RTViewMgr view(vp, vrp, angle, ds);
// 物体(球)データの設定 (標準入力からデータ入力)
	cin >> obj_num;				 //球の数を入力
	for(i = 0; i < obj_num; i++){
		cin >> cnt.c[0] >> cnt.c[1] >> cnt.c[2];
	//cin >> cnt.c[0] >> cnt.c[1] >> cnt.c[2]; // 球の中心の入力
		cin >> rad;                              // 球の半径の入力
		cin >> kd;                               // 拡散反射率の入力
		cin >> ka;				 // 環境反射率の入力
        	cin >> ks;				 // 鏡面反射率の入力
		cin >> n;				 // 鏡面反射べき指数の入力
		obj.push_front(new RTObject(cnt, rad, kd, ka, ks, n));
	}
//環境光の強さ
	cin >> ia;
//拡散反射率の設定
	cin >> dir_num;
	for(i = 0; i < dir_num; i++){
		cin >> dir.c[0] >> dir.c[1] >> dir.c[2];
		cin >> it;
		line.push_front(new RTLSrc(dir, it));
	}

//
// 各ピクセルの輝度の計算と標準出力への出力
	view.prnpgmhdr();	// PGMファイルのヘッダ出力
	int hit_min_flag = 0;
	RTRay ray;
	RTHit hit;
	RTHit hit_min;
	for(int y = 2 * ds - 1; y >= 0; y--){
		for(int x = 0; x < 2 * ds; x++) {
			double mint = 5000;
			for(RTObject* o: obj){
				view.getray(x, y, &ray); // ピクセルへの光線を得る
				if(o->rayhit(ray, &hit) && hit.g_t() < mint){
					mint = hit.g_t();
					hit_min = hit;
					hit_min_flag = 1;
				}
			}
				//if(obj.rayhit(ray, &hit))
			if(hit_min_flag == 1){
				cout << hit_min.intensity(line, ia);
			}
			else {
				cout << static_cast<unsigned char>(0);
			}
			hit_min_flag = 0;
		}
	}
}
