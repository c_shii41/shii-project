/*プログラミングR13 */
/* Created by 石田日々輝 on 2017/06/21 */
/* 方程式f(x)=cosx-x=0は(0,1)にただ一つの解を持つ。
二分法を用いて誤差±10^-3以内でこの解を求めよ。 */
#include <stdio.h>
#include <math.h>
#define EPS 2e-3
int main (void)
{
    double x1,x2,x,y;

    x1=0;
    x2=1;
    while(x2-x1>EPS){
        x=(x1+x2)/2;
        y=cos(x)-x;
        if(y>0){
            x1=x;
        }
        if(y<0){
            x2=x;
        }
    }
    printf("x=%lf\n",(x1+x2)/2);

    return 0;
}