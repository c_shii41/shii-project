import socket
import concurrent.futures
from sys import exit

# Socket通信初期化
host = '0.0.0.0'
port = 60103
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
try:
    s.connect((host, port))
except ConnectionRefusedError as e:
    print('サーバー側から接続拒否されました．')
    exit(1)
except Exception as e:
    print(e)
    exit(1)


# サーバーからのデータ受信用関数
def recv():
    while True:
        try:
            # データを受信して出力
            res = s.recv(4096)
            if res.decode() == 'eNdCoNnEcTiOn':
                print('Good Bye')
                return
            if not res:
                print('サーバー側からのデータが異常です．\n'
                      'Enterキーを押下してください．\n'
                      '(受信処理を停止しました)')
                return
            print(res.decode())
        except Exception as e:
            # 例外発生時の処理
            print(e)
            print('異常切断されました．')
            return


# サーバーへのデータ送信用関数
def send():
    print('何も入力せずEnterキー押下で終了します．')
    while True:
        # ユーザーからのコンソール入力
        str_input = input('>> ')
        try:
            # サーバへ送信
            s.send(str_input.encode('UTF-8'))
            # 無入力でのEnterキー押下時の終了処理
            if not str_input:
                s.send('eNdCoNnEcTiOn'.encode('UTF-8'))
                print('切断します．')
                return
        except Exception as e:
            # 例外発生時の処理
            print(e)
            print('異常切断されました．')
            return


# Main
if __name__ == "__main__":
    try:
        print('サーバーからの応答をお待ちください．．．\n'
                '(別ユーザーが設定中です)')
        print(s.recv(4096).decode())
        str_input = input('ユーザー名>> ')
        if not str_input:
            str_input = '名無し'
        s.send(str_input.encode('UTF-8'))
        start_flag = True
    except Exception as e:
        print(e)
        print('ユーザー設定にて問題が起きました．\n'
                'もう一度試してください．')
        error_flag = True

    # スレッドプールを用いた並列処理
    executor = concurrent.futures.ThreadPoolExecutor(max_workers=2)
    try:
        executor.submit(recv)
        executor.submit(send)
    except Exception as e:
        print(e)
