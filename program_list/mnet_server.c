#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>

#define PORT 19991

int gcd(int x, int y) {
  int t;
  while (x > 0) {
    if (x < y) { t = x; x = y; y = t; }
    x = x - y;
  }
  return y;
}

main()
{
  int i, s, t, len, x, y, pid;
  struct sockaddr_in saddr;
  struct sockaddr_in caddr;
  char   buf[100];

  if ((s = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
    perror("socket");
    exit(1);
  }

  saddr.sin_family = AF_INET;
  saddr.sin_addr.s_addr = INADDR_ANY;
  saddr.sin_port = htons(PORT);

  if (bind(s, (struct sockaddr *)&saddr, sizeof(struct sockaddr_in)) < 0) {
    perror("bind");
    exit(1);
  }

  if (listen(s,1) < 0) {
    perror("listen");
    exit(2);
  }

  for (;;) {
    len = sizeof(caddr);
    if ((t = accept(s, (struct sockaddr *)&caddr, &len)) < 0) {
      perror("accept");
      exit(3);
    }

    if ((pid = fork()) < 0) {
      perror("fork");
      exit(1);
    } else if (pid == 0) {
      close(s);
      while (len = read(t, buf, 100)) {
	if (strncmp(buf, "quit", 4) != 0) { 
	  sscanf(buf, "%d %d", &x, &y); 
	  sprintf(buf, "gcd(%d,%d)=%d\n", x, y, gcd(x,y));
	  write(t, buf, 100);
	}
      }
      exit(0);
    }
    close(t);
  }
}
