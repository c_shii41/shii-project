/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javacalc;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

/**
 *
 * @author isshi
 */

public class FXMLDocumentController implements Initializable {
    
    enum Operater {
        none, add, sub, mul, div, equal, pow
    };
    Operater ope = Operater.none;
    Operater memoope = Operater.none;

    enum SubOpe {
        none, log_eX, log_10X, slog_XY, sin, cos, tan, sqrt
    };
    SubOpe sope = SubOpe.none;
    
    public class calculate {
        //変数宣言
        private BigDecimal reg = new BigDecimal("0");
        private BigDecimal register = new BigDecimal(0);
        private BigDecimal accum = new BigDecimal(0);
        private BigDecimal result = new BigDecimal(0);
        private BigDecimal memoaccum = new BigDecimal(0);
        private BigDecimal[] memofomula = new BigDecimal[100];
        private BigDecimal π = new BigDecimal(Math.PI);
        private BigDecimal e = new BigDecimal(Math.E);
        private BigDecimal a = new BigDecimal(0);
        private BigDecimal one = new BigDecimal(1);
        private BigDecimal pai = new BigDecimal(180);
        private BigDecimal Angle = one.divide(pai, 20, BigDecimal.ROUND_HALF_UP);
        private BigDecimal AngletoRad = π.multiply(Angle);

        private boolean point_flag = false;
        private boolean none_flag = false;
        private boolean logeX_flag = false;
        private boolean log10X_flag = false;
        private boolean logXY_flag = false;
        private boolean sin_flag = false;
        private boolean cos_flag = false;
        private boolean tan_flag = false;
        private boolean sqrt_flag = false;
        private boolean divide_zero_flag = false;
        private int left_count = 0;
        private int right_count = 0;
        private int point_pos = 1;
        private double pow_count;
        private double pow_ans;
        private double pow_bottom;
        //double pow_ans;
        //private double pow_bottom;
        //double pow_ans;
        //private double pow_ans;
        private double logeX;
        private double log10X;
        private double logXY;
        private double sinX;
        private double cosX;
        private double tanX;
        private double sqrtX;
        
        public void s_flag_reset() {
            this.logeX_flag = false;
            this.log10X_flag = false;
            this.logXY_flag = false;
            this.sin_flag = false;
            this.cos_flag = false;
            this.tan_flag = false;
            this.sqrt_flag = false;
            this.divide_zero_flag = false;
        }

        public void s_memofomula(BigDecimal[] memofomula) {this.memofomula = memofomula;}
        public void s_π(BigDecimal π) {this.π = π;}
        public void s_e(BigDecimal e) {this.e = e;}
        public void s_one(BigDecimal one) {this.one = one;}
        public void s_pai(BigDecimal pai) {this.pai = pai;}
        public void s_Angle(BigDecimal Angle) {this.Angle = Angle;}
        public void s_AngletoRad(BigDecimal AngletoRad) {this.AngletoRad = AngletoRad;}
        public void s_left_count(int left_count) {this.left_count = left_count;}
        public void s_right_count(int right_count) {this.right_count = right_count;}
        public void s_reg(BigDecimal reg) {this.reg = reg;}
        public void s_register(BigDecimal register) {this.register = register;}
        public void s_accum(BigDecimal accum) {this.accum = accum;}
        public void s_result(BigDecimal result) {this.result = result;}
        public void s_memoaccum(BigDecimal memoaccum) {this.memoaccum = memoaccum;}
        public void s_a(BigDecimal a) {this.a = a;}
        public void s_point_flag(boolean point_flag) {this.point_flag = point_flag;}
        public void s_none_flag(boolean none_flag) {this.none_flag = none_flag;}
        public void s_logeX_flag(boolean logeX_flag) {this.logeX_flag = logeX_flag;}
        public void s_log10X_flag(boolean log10X_flag) {this.log10X_flag = log10X_flag;}
        public void s_logXY_flag(boolean logXY_flag) {this.logXY_flag = logXY_flag;}
        public void s_sin_flag(boolean sin_flag) {this.sin_flag = sin_flag;}
        public void s_cos_flag(boolean cos_flag) {this.cos_flag = cos_flag;}
        public void s_tan_flag(boolean tan_flag) {this.tan_flag = tan_flag;}
        public void s_sqrt_flag(boolean sqrt_flag) {this.sqrt_flag = sqrt_flag;}
        public void s_divide_zero_flag(boolean divide_zero_flag) {this.divide_zero_flag = divide_zero_flag;}
        public void s_point_pos(int point_pos) {this.point_pos = point_pos;}
        public void s_pow_count(double pow_count) {this.pow_count = pow_count;}
        public void s_pow_bottom(double pow_bottom) {this.pow_bottom = pow_bottom;}
        public void s_pow_ans(double pow_ans) {this.pow_ans = pow_ans;}
        public void s_logeX(double logeX) {this.logeX = logeX;}
        public void s_log10X(double log10X) {this.log10X = log10X;}
        public void s_logXY(double logXY) {this.logXY = logXY;}
        public void s_sinX(double sinX) {this.sinX = sinX;}
        public void s_cosX(double cosX) {this.cosX = cosX;}
        public void s_tanX(double tanX) {this.tanX = tanX;}
        public void s_sqrtX(double sqrtX) {this.sqrtX = sqrtX;}
        
        public BigDecimal g_reg() {return reg;}
        public BigDecimal g_register() {return register;}
        public BigDecimal g_accum() {return accum;}
        public BigDecimal g_result() {return result;}
        public BigDecimal g_memoaccum() {return memoaccum;}
        public BigDecimal[] g_memofomula() {return memofomula;}
        public BigDecimal g_π() {return π;}
        public BigDecimal g_e() {return e;}
        public BigDecimal g_a() {return a;}
        public BigDecimal g_one() {return one;}
        public BigDecimal g_pai() {return pai;}
        public BigDecimal g_Angle() {return Angle;}
        public BigDecimal g_AngletoRad() {return AngletoRad;}
        public boolean g_point_flag() {return point_flag;}
        public boolean g_none_flag() {return none_flag;}
        public boolean g_logeX_flag() {return logeX_flag;}
        public boolean g_log10X_flag() {return log10X_flag;}
        public boolean g_logXY_flag() {return logXY_flag;}
        public boolean g_sin_flag() {return sin_flag;}
        public boolean g_cos_flag() {return cos_flag;}        
        public boolean g_tan_flag() {return tan_flag;}
        public boolean g_sqrt_flag() {return sqrt_flag;}
        public boolean g_divide_zero_flag() {return divide_zero_flag;}
        public int g_left_count() {return left_count;}
        public int g_right_count() {return right_count;}
        public int g_point_pos() {return point_pos;}
        public double g_pow_count() {return pow_count;}
        public double g_pow_bottom() {return pow_bottom;}
        public double g_pow_ans() {return pow_ans;}
        public double g_logeX() {return logeX;}
        public double g_log10X() {return log10X;}
        public double g_logXY() {return logXY;}
        public double g_sinX() {return sinX;}
        public double g_cosX() {return cosX;}
        public double g_tanX() {return tanX;}
        public double g_sqrtX() {return sqrtX;}
    }
    
    calculate calc = new calculate();
    
   
    //フラグのリセット(一部除く)
    public void flag_reset(){
        calc.s_flag_reset();
    }    
    
    //テキストの表示
    @FXML
    private TextField display;
    
    //左括弧の認識
    @FXML
    private void handleLeftP(ActionEvent event) {
        calc.left_count++;
        if (ope != Operater.none) {
            memoope = ope;
            ope = Operater.none;
            calc.s_memoaccum(calc.g_accum());
            calc.s_accum(new BigDecimal(0));
            calc.s_none_flag(true);
        }
    }
    
    //右括弧の認識
    @FXML
    private void handleRightP(ActionEvent event) {
        calc.right_count++;
        if (calc.g_left_count() >= calc.g_right_count() && calc.g_none_flag()) {
            calc();
            clear();
            ope = memoope;
            calc.s_register(calc.g_memoaccum());
        }
        else{
            calc();
            calc.register = new BigDecimal("0");
            clear();
        }
        calc.left_count = 0;
        calc.right_count = 0;
    }
    
    //小数点の認識
    @FXML
    private void handlePointAction(ActionEvent event) {
        calc.s_point_flag(true);
        display.setText(calc.g_register().toPlainString() + ".");
    }
    
    //数字を入力、表示、記憶
    @FXML
    private void handleNumberAction(ActionEvent event) {
        Button b = (Button)event.getSource();
        BigDecimal val = new BigDecimal(b.getText());

        if (calc.g_point_flag()) {
            for(int i = 0; i < calc.g_point_pos(); i++){
                val = val.multiply(new BigDecimal("0.1"));
            }
            calc.point_pos++;
        }
        else {
            calc.s_reg(calc.g_reg().multiply(new BigDecimal("10")));
        }
        calc.s_reg(calc.g_reg().add(val));
        
        calc.s_register(calc.g_reg());
        display.setText(calc.g_register().toPlainString());
    }
    
    //オールクリアの動作
    @FXML
    private void handleACAction(ActionEvent event) {
        clear();
        calc.s_register(new BigDecimal("0"));
        calc.s_point_flag(false);
        flag_reset();
        ope = Operater.none;
        sope = SubOpe.none;
        display.setText("0");
    }
    
    //計算の中心部
    private void calc(){
        //BigDecimal NumtoBD = new BigDecimal(display.getText());
        switch (ope) {
            case none:
                calc.s_accum(calc.g_register());
                if(calc.g_accum() == calc.g_π() || calc.g_accum() == calc.g_e()){
                    break;
                }
                return;
            case add:
                calc.s_accum(calc.g_accum().add(calc.g_register()));
                break;
            case sub:
                calc.s_accum(calc.g_accum().subtract(calc.g_register()));
                break;
            case mul:
                calc.s_accum(calc.g_accum().multiply(calc.g_register()));
                break;
            case div:
                if(calc.g_register().doubleValue() == 0){
                    calc.s_divide_zero_flag(true);
                    return;
                }
                calc.s_accum(calc.g_accum().divide(calc.g_register(), 9, BigDecimal.ROUND_HALF_UP));
                break;
            case pow://一度double型に戻してから冪乗する
                calc.s_pow_count(calc.g_register().doubleValue());
                calc.s_pow_bottom(calc.g_accum().doubleValue());
                calc.s_pow_ans(Math.pow(calc.g_pow_bottom(), calc.g_pow_count()));
                calc.s_accum(new BigDecimal(calc.g_pow_ans()));
                break;
            case equal:
                break;
        }
        //計算結果は小数点第10位固定にする。
        calc.s_result(calc.g_accum().setScale(9, RoundingMode.HALF_UP));
        display.setText(calc.g_result().stripTrailingZeros().toPlainString());
    }
    
    //追加機能の計算の中心部
    //全て一度double型に戻し、Math.を用いている。
    private void calc2(){
        switch (sope) {
            case none:
                break;
            case log_eX:
                calc.s_logeX(Math.log(calc.g_register().doubleValue()));
                calc.s_register(new BigDecimal(calc.g_logeX()));
                calc.s_register(calc.g_register().setScale(9, RoundingMode.HALF_UP));
                break;
            case log_10X:
                calc.s_log10X(Math.log10(calc.g_register().doubleValue()));
                calc.s_register(new BigDecimal(calc.g_log10X()));
                calc.s_register(calc.g_register().setScale(9, RoundingMode.HALF_UP));
                break;
            case slog_XY:
                calc.s_logXY(Math.log(calc.g_register().doubleValue()) / Math.log(calc.g_a().doubleValue()));
                calc.s_register(new BigDecimal(calc.g_logXY()));
                calc.s_register(calc.g_register().setScale(9, RoundingMode.HALF_UP));
                break;
            case sin:
                calc.s_register(calc.g_AngletoRad().multiply(calc.g_register()));
                calc.s_sinX(Math.sin(calc.g_register().doubleValue()));
                calc.s_register(new BigDecimal(calc.g_sinX()));
                calc.s_register(calc.g_register().setScale(9, RoundingMode.HALF_UP));
                break;
            case cos:
                calc.s_register(calc.g_AngletoRad().multiply(calc.g_register()));
                calc.s_cosX(Math.cos(calc.g_register().doubleValue()));
                calc.s_register(new BigDecimal(calc.g_cosX()));
                calc.s_register(calc.g_register().setScale(9, RoundingMode.HALF_UP));
                break;
            case tan:
                calc.s_register(calc.g_AngletoRad().multiply(calc.g_register()));
                calc.s_tanX(Math.tan(calc.g_register().doubleValue()));
                calc.s_register(new BigDecimal(calc.g_tanX()));
                calc.s_register(calc.g_register().setScale(9, RoundingMode.HALF_UP));
                break;
            case sqrt:
                calc.s_sqrtX(Math.sqrt(calc.g_register().doubleValue()));
                calc.s_register(new BigDecimal(calc.g_sqrtX()));
                calc.s_register(calc.g_register().setScale(9, RoundingMode.HALF_UP));
                break;
        }
        display.setText(calc.g_register().stripTrailingZeros().toPlainString());
    }
    
    //階乗計算の中心部
    static int factorial(int n){
        int fact = 1;
        if (n == 0){
            return fact;
        }
        else{
            for(int i = n; i > 0; i--){
                fact *= i;
            }
            return fact;
        }
    }
    
    //階乗の認識
    @FXML
    private void handleFactorialAction(ActionEvent event) {
        int n = calc.g_register().intValue();
        calc.s_register(new BigDecimal(factorial(n)));
    }
    
    //πの認識
    @FXML
    private void handlePIAction(ActionEvent event) {
        calc.s_register(calc.g_π());
        display.setText("π");   
    }
    
    //eの認識
    @FXML
    private void handleNapierAction(ActionEvent event) {
        calc.s_register(calc.g_e());
        display.setText("e");
    }
    
    //log(X)の計算のフラグ
    @FXML 
    private void handleLog_eX(ActionEvent event) {
        calc.s_logeX_flag(true);
        calc2();
        clear2();
        sope = SubOpe.log_eX;
    }
    
    //log10(X)の計算のフラグ
    @FXML 
    private void handleLog_10X(ActionEvent event) {
        calc.s_log10X_flag(true);
        calc2();
        clear2();
        sope = SubOpe.log_10X;
    }
    
    //logX(Y)の計算のフラグ
    @FXML
    private void handleLog_XY(ActionEvent event) {
        calc.s_logXY_flag(true);
        calc.s_a(calc.g_register());
        calc2();
        clear2();
        sope = SubOpe.slog_XY;
    }
    
    //sin(X)の計算のフラグ
    @FXML
    private void handleSinAction(ActionEvent event) {
        calc.s_sin_flag(true);
        calc2();
        clear2();
        sope = SubOpe.sin;
    }
    
    //cos(X)の計算のフラグ
    @FXML
    private void handleCosAction(ActionEvent event) {
        calc.s_cos_flag(true);
        calc2();
        clear2();
        sope = SubOpe.cos;
    }
    
    //tan(X)の計算のフラグ
    @FXML
    private void handleTanAction(ActionEvent event) {
        calc.s_tan_flag(true);
        calc2();
        clear2();
        sope = SubOpe.tan;
    }
    
    //√(X)の計算のフラグ
    @FXML
    private void handleSQRTAction(ActionEvent event) {
        calc.s_sqrt_flag(true);
        calc2();
        clear2();
        sope = SubOpe.sqrt;
    }
    
    //冪数計算
    @FXML
    private void handleXpowY(ActionEvent event) {
        calc2();
        calc();
        clear();
        ope = Operater.pow;
    }
    
    //加算
    @FXML
    private void handleAddAction(ActionEvent event) {
        calc2();
        calc();
        clear();
        ope = Operater.add;
    }
    
    //減算
    @FXML
    private void handleSubAction(ActionEvent event) {
        calc2();
        calc();
        clear();
        ope = Operater.sub;
    }
    
    //乗算
    @FXML
    private void handleMulAction(ActionEvent event) {
        calc2();
        calc();
        clear();
        ope = Operater.mul;
    }
    
    //除算
    @FXML
    private void handleDivAction(ActionEvent event) {
        calc2();
        calc();
        clear();
        ope = Operater.div;
    }

    //計算実行とフラグの初期化
    @FXML
    private void handleEQAction(ActionEvent event) {
        calc2();
        calc();
        if(calc.g_divide_zero_flag() == true){
            calc.s_result(new BigDecimal(0));
            display.setText("Error : Divide by Zero");
            clear();
            calc.s_register(new BigDecimal("0"));
            calc.s_accum(new BigDecimal("0"));
            ope = Operater.none;
            sope = SubOpe.none;
            return;
        }
        clear();
        ope = Operater.equal;
        flag_reset();
    }
    
    //クリア
    private void clear() {
        calc.s_reg(new BigDecimal("0"));
        //register = new BigDecimal("0");
        calc.s_point_pos(1);
        calc.s_point_flag(false);
        flag_reset();
        sope = SubOpe.none;
    }
    
    //特別な場合のクリア
    private void clear2() {
        calc.s_reg(new BigDecimal("0"));
        calc.s_register(new BigDecimal("0"));
        calc.s_point_pos(1);
        calc.s_point_flag(false);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        //To change body of generated methods, choose Tools | Templates.
    }
}
