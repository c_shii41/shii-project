#include <stdio.h>
#include <unistd.h>

main()
{
  int fds[2];

  pipe(fds);
  if (fork()) {
    if (fork()) {
      close(fds[0]); close(fds[1]); wait(); wait();
    } else {
      close(0); dup(fds[0]); close(fds[0]);
      close(fds[1]);
      execl("/usr/bin/wc", "wc", NULL);
    }
  } else {
    close(1);
    dup(fds[1]);
    close(fds[0]);
    close(fds[1]);
    execl("/bin/ls", "ls", "-l", ".", NULL);
  }
}
