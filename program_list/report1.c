/* frksmpl1.c */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>

int main(){
  int pid, i, j;
  int p_id = getpid();

  /*子プロセスを3個生成*/
  for(j = 0; j < 3; j++){
    pid = fork();
    
    /* フォーク失敗 */
    if(pid == -1){
      fprintf(stderr, "fork has failed.\n");
    }
    /* 子プロセス */
    else if(pid == 0){
	printf("I am a child process.\n");
	printf("PPID = %d\n", getppid());
	printf(" PID = %d\n", getpid());
	sleep(2);
    }
    /* 親プロセス */
    else{
	//printf("I am a parent process.\n");
	sleep(3);
    }

    /* 子プロセスのPPIDと親プロセスのPIDが等しいときbreak */
    /* True -> その子プロセスの動作を終了 */
    if(getppid() == p_id){
      break;
    }
  }
  return 0;
}
    
