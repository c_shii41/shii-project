import socket
import threading
from sys import exit

# ソケット初期化
host = '0.0.0.0'
port = 61114
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((host, port))
s.listen(10)

# 接続中のクライアントの情報を格納するリスト
client_info = []
# 接続中のユーザの名前を保存するリスト
client_name = []

def handler(connection, address, user_name) :
    discom_flag = True
    while True :
        if not discom_flag :
            return

        try :
            res = connection.recv(4096)
            if not res or res.decode() == "*logout" :
                connection.send("*logout".format(user_name).encode("UTF-8"))
                connection.close()
                print("*** 接続終了 ***")
                print(" アドレス       : {}".format(address[0]))
                print(" ポート番号     : {}".format(address[1]))
                print(" ユーザーネーム : {}".format(user_name))
                client_info.remove((connection, address, user_name))
                client_name.remove(user_name)
                discom_flag = False

            if res.decode() == "*name" :
                for i, name in enumerate(client_name, 1) :
                    name_data = "{} : {}".format(i, name)
                    connection.send(name_data.encode("UTF-8"))

            for clients in client_info :
                ClientAddress = clients[1][0]
                ClientPort = clients[1][1]
                recv_Address = address[0]
                recv_Port = address[1]
                if ClientAddress == recv_Address and ClientPort == recv_Port :
                    print("{} : {}".format(user_name, res.decode()))
                else :
                    clients[0].send("{} : {}".format(user_name, res.decode()).encode("UTF-8"))
        except Exception as error :
            print(error)
            return

# ユーザーネームの設定
def client_setting(connection, address) :
    while True :
        connection.send('ユーザーネームを入力してください。\n'.encode("UTF-8"))
        res = connection.recv(4096)
        user_name = res.decode()
        break
    
    print("*** 新規接続 ***")
    print(" アドレス       : {}".format(address[0]))
    print(" ポート番号     : {}".format(address[1]))
    print(" ユーザーネーム : {}".format(user_name))

    client_info.append((connection, address, user_name))
    client_name.append(user_name)
    connection.send("{} << ログインしました。".format(user_name).encode("UTF-8"))

    thread_handler = threading.Thread(
        target = handler,
        args = (connection, address, user_name)
    )
    thread_handler.start()

    return

# Main Function
if __name__ == "__main__" :
    while True :
        try :
            # クライアントからの接続要求の受付と接続
            connection, address = s.accept()

            #接続したクライアントの設定とマルチスレッドの開始
            thread_setting = threading.Thread(
                target = client_setting, 
                args = (connection, address)
            )
            thread_setting.start()

        except Exception as error :
            print(error)
            break

        except KeyboardInterrupt :
            print(" Interrupted")
            exit(0)