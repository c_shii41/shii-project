/**********************************************************************/
/* 配列を用いたスタックの実現                                         */
/**********************************************************************/
#include <stdio.h>              /* getchar(), putchar(), printf() */
#include <stdlib.h>             /* exit() */

#define STACKSIZE 100           /* スタックサイズ */
typedef struct {
    char kind;
    int line;
    int pos;
} elem_t;

typedef struct {
   elem_t data[STACKSIZE];     /* データ格納領域 */
   int    top;                 /* スタックトップの添字位置 */
} st_t;                      /* スタック型     */

/**********************************************************************/
/* 関数のプロトタイプ宣言                                             */
/**********************************************************************/
void   initStack(st_t *st);
void   push(elem_t x, st_t *st);
elem_t pop(st_t *st);
elem_t peekStack(const st_t *st);
int    isEmptyStack(const st_t *st);
int    isFullStack(const st_t *st);
void   printStack(const st_t *st);

/**********************************************************************/
/* スタック操作関数定義                                               */
/**********************************************************************/
void initStack(st_t *st) {
   st->top = -1;               /* 空のスタックの top は -1 とする */
}

elem_t peekStack(const st_t *st) {
   if (isEmptyStack(st)) {
       printf("PeekStack Error: Stack Empty\n");
       exit(1);
   }
   return st->data[st->top];
}
void push(elem_t x, st_t *st){
	st -> top++;
	st -> data[st -> top] = x;
}

elem_t pop(st_t *st){
	elem_t c;

	c = st -> data[st -> top];
	st -> top--;
	
	return c;
}

int isEmptyStack(const st_t *st){
	if(st -> top == -1){
		return 1;
	}
	else
		return 0;
}

int isFullStack(const st_t *st){
	if(st -> top == STACKSIZE - 1){
		return 1;
	}
	else
		return 0;
}

/**********************************************************************/
/* メインプログラム                                                   */
/**********************************************************************/
int main() {
   char c;
   st_t st;
   int i, line = 1, pos = 1;
   elem_t checker,popchecker;

   initStack(&st);                     /* スタックの初期化 */

   printf("input line : ");
   while ((c = getchar()) != EOF) {   /* 改行になるまで一文字ずつ読込んで*/
       if(c == '(' || c == '['){
           checker.kind = c;
           checker.line = line;
           checker.pos = pos;
           push(checker, &st);         /* 文字をスタックにプッシュ */
       }
       if(c == ')' || c == ']'){
           if(isEmptyStack(&st)){
                 printf("閉じ括弧が余分です。\n");
                 break;
           }
           popchecker = pop(&st);
           printf("%c,%c",popchecker.kind,c);
              if(popchecker.kind == '(' && c == ')'){
                 pos++;
                 printf("%d\n",pos);
                 continue;
              }
              if(popchecker.kind == '[' && c == ']'){
                 pos++;
                 printf("%d\n",pos);
                 continue;
              }
              if((popchecker.kind == '(' && c == ']') || (popchecker.kind == '[' && c == ')')){
                 printf("%d行目%d列目と%d行目%d列目の閉じ括弧が非対応です。\n", popchecker.line, popchecker.pos, line, pos);
                 break;
              }
       }
       pos++;
       printf("%d\n",pos);
   }
   if(isEmptyStack(&st) == 1 && c == EOF){
           printf("エラーはありません。\n");
           return 0;
   }
   if(c == EOF)
           printf("開き括弧が余分です。\n");

   return 0;
}