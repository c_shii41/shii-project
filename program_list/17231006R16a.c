#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main(void)
{
    int i,j,n,x,a[5][5],b[100];

    x=1;
    for(i=0;i<100;i++)
        b[i]=1;
    srand(time(NULL));
    for(i=0;i<5;i++){
        for(j=0;j<5;j++){
            a[i][j]=rand() %99+1;
            if(b[a[i][j]]==1){
                b[a[i][j]]=0;
                printf("%2.1d ",a[i][j]);
            }
            else
                j--;
        }
        printf("\n");
    }
    printf("1から99までの数字を入力してください.");
    printf("n="); scanf("%d",&n);
    for(i=0;i<5;i++)
        for(j=0;j<5;j++){
            if(a[i][j]==n){
                printf("%dは(%d,%d)成分です.\n",n,i+1,j+1);
                x=0;
                break;
            }
        }
    if(x==1)
        printf("%dはこの配列の成分ではありません.\n",n);
    return 0;
}