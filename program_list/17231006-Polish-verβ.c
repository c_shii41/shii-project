#include <stdio.h>
#include "stack.h"
#include <string.h>
#include <ctype.h>
#include <stdlib.h>

int errorFlag = 0; //エラーフラグ　0:正常 1:異常
int check = 1; //スタックの空状態チェッカー 1:スタックあり 0:空状態


// char型の文字を数字に変換する関数
int num1(char n){
    return atoi(&n);
}

void error(){
    errorFlag = 1;
}

void stackError(){
    error();
    check = 0;
    }

// 入力された式から10進数を抽出する関数
int number(char *expr, int *next){
    int value;

    if(!isdigit(expr[*next])){
        exit(1);
    }
    else{
        value = num1(expr[*next]);
        (*next)++;
        while(isdigit(expr[*next])){
            value = value * 10 + num1(expr[*next]);
            (*next)++;
        }
    }
    return value;
}

int poper(int *comma, int *pos, int *a, int *b, stack *s){
    if(!isempty(s)){
        *a = pop(s);
    }
    else{
        stackError();
        return 0;
    }
    if(!isempty(s)){
        *b = pop(s);
    }
    else{
        stackError();
        return 0;
    }
    *comma = 0;
    (*pos)++;

    return 0;
}

int valpolish(char *expr){
    int pos = 0;
    int comma = 1;
    int a, b;
    int answer;
    stack s;
    makenull(&s);

    while(1){
        if(isdigit(expr[pos]) && errorFlag == 0 && comma == 1){
            comma = 0;
            push(number(expr, &pos), &s);
        }
        else if(expr[pos] == '+' && errorFlag == 0 && comma == 1){ //加法演算 
            poper(&comma, &pos, &a, &b, &s);
            push(a + b, &s);
        }
        else if(expr[pos] == '-' && errorFlag == 0 && comma == 1){ //減法演算
            poper(&comma, &pos, &a, &b, &s);
            push(b - a, &s);
        }
        else if(expr[pos] == '*' && errorFlag == 0 && comma == 1){ //乗法演算
            poper(&comma, &pos, &a, &b, &s);
            push(a * b, &s);
        }
        else if(expr[pos] == '/' && errorFlag == 0 && comma == 1){ //除法演算
            poper(&comma, &pos, &a, &b, &s);
            push(b / a, &s);
        }
        else if(expr[pos] == ','){ //文法チェッカー
            if(pos == 0){
                error();
                printf("式の最初が','になっています。\n");
                return 0;
            }
            comma++;
            pos++;
        }
        else if(expr[pos] == 'm' && errorFlag == 0 && comma == 1){ //正負逆転演算
            comma = 0;
            if(!isempty(&s)){
                a = pop(&s);
            }
            else{
                error();
                stackError();
                return 0;
            }
            a = a * -1;
            push(a, &s);
            pos++;
        }
        else if(expr[pos] == '\n' && errorFlag == 0 && comma == 0){ //逆ポーランド式入力終了地点
            if(!isempty(&s)){
                answer = pop(&s);
            }
            if(isempty(&s)){
                return answer;
            }
            else{
                error();
                printf("演算子が不足しています。\n");                        
                return 0;
            }
        }
        else if(expr[pos] == ' '){ //式中に空白がある
            error();
            printf("式中に空白記号があります。空白記号は入力しないでください。\n");
            return 0;
        }
        else{
            if(expr[pos] == '\n' && comma == 1 && check == 1){
                error();
                printf("式が正しくありません。\n");
            }
            else if(comma == 0 && check == 1){
                error();
                printf("演算子の間に','が使われていない箇所があります。\n");
            }
            else if(comma >= 2 && check == 1){
                error();
                printf("数字または演算子の間に','が複数使われている箇所があります\n");
            }
            else if(check == 0){
                error();
                printf("スタックが空状態です。数字が少なすぎます。\n");
            }
            else if(check == 1){
                error();
                printf("数字、演算子、','以外の文字が式中に使われている箇所があります。\n");
            }
            return 0;
        }
    }
}


int main(void){
    int sum;
    char value[200];

    while(1){
        errorFlag = 0;
        check = 1;
        printf("逆ポーランド式を入力してください。\n");
        printf("Enterキーで式を確定します。\n");
        printf("何も入力せずEnterキーを入力するとプログラムを終了します。\n");

        fgets(value, 200, stdin);

        if(value[0] != '\n'){
            sum = valpolish(value);
        }

        if(value[0] == '\n'){
            printf("式が入力されなかったため、逆ポーランド演算を終了します。\n");
            return 0;
        }

        else if(errorFlag == 0){
            printf("演算結果: %d\n\n", sum);
        }

        else{
            printf("式を再入力してください。\n\n");
        }
    }
    return 0;
}