import socket
import select
import threading
import sys

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# 接続待ちするサーバのホスト名とポート番号を指定
host = "0.0.0.0"
port = 55565
argument = (host, port)
sock.bind(argument)
# 5 ユーザまで接続を許可
sock.listen(5)
clients = []


# 接続済みクライアントは読み込みおよび書き込みを繰り返す
def loop_handler(connection, address):
    loop = True
    first = 0
    end_sock = 0
    while loop:
        try:
            #クライアント側から受信する
            if first == 0:
                name = connection.recv(4096)
                first = 1
            res = connection.recv(4096)
            if not res:
                break
            for value in clients:
                if value[1][0] == address[0] and value[1][1] == address[1] :
                    if res.decode() == 'logout':
                        #value[0].send("->{} : {}".format(name.decode(), res.decode()).encode("UTF-8"))
                        print("{} : ログアウトしました。\n".format(name.decode()))
                        #connection.shutdown(socket.SHUT_RDWR)
                        #value[0].close()
                        end_sock = 1
                        break
                    elif end_sock == 0 :
                        print("[アクセス元アドレス]=>{}, [アクセス元ポート]=>{}".format(address[0], address[1]))
                        print("{} : {}".format(name.decode(), res.decode()))
                        print("")
                else :
                    value[0].send("->{} : {}".format(name.decode(), res.decode()).encode("UTF-8"))
        except Exception as e:
            print(e)
            break

while True:
    try:
        # 接続要求を受信
        conn, addr = sock.accept()
    except KeyboardInterrupt:
        sys.exit()
        #for tmp in clients:
            #tmp[0].send("keyboardInterrupt".encode("UTF-8"))
            #tmp[0].shutdown(socket.SHUT_RDWR)
            #if not tmp[0]:
                #tmp[0].close()
            #print("")
            #print(tmp[1][1],'をシャットダウンしました.')                
            #sys.exit()

    # 待受中にアクセスしてきたクライアントを追加
    clients.append((conn, addr))
    # スレッド作成
    thread = threading.Thread(target = loop_handler, args = (conn, addr), daemon = True)
    # スレッドスタート
    thread.start()