/* 課題
中置記法式を与えると，その値を計算する関数　　　　　
int valinfix(char *expr);
を作成し，適当なメインルーチンも作成して動作できるプログラムを完成すること．
中置記法の構文と意味もレポートに含めること．
*/

#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>

#define MAXLENGTH 100

int ERR = 0; //エラー有りで1にセット

void error(char *expr, int p, char *message){
    int i;

    fprintf(stderr, "%s\n", expr); //式を表示
    for(i = 0; i < p; i++){ //エラーが見つかった位置を表示
        putc(' ', stderr);
    }
    putc('^', stderr);
    putc('\n', stderr);
    fprintf(stderr, "error: %s\n", message); //エラーメッセージを表示
    ERR = 1; // エラーフラグをセット
}

//数字1文字を対応する0~9の数に変換
int num1(char n){
    return (n - '0');
}

//数の処理
//isdigit : 10進数なら真
int number(char *expr, int *p){
    int value = 0;

    if(!isdigit(expr[*p]) && ERR == 0){
        error(expr,*p,"数字がありません。");
        return 0;
    }
    else{
        value = num1(expr[*p]);
        (*p)++;
        while(isdigit(expr[*p])){
	    value = value * 10 + num1(expr[*p]);
	    (*p)++;
        }
    }
    return value;
}

//式の処理(再帰)
int valinfix0(char *expr, int *p){
  int x, y, i,  answer = 0; //部分式の評価結果
    char op; //演算子
    
    if(expr[*p] != '('){
        return number(expr, p);
    }
    else{
        (*p)++;
	
	if(expr[*p] == '-'){
            (*p)++;
            i = valinfix0(expr, p);
            (*p)++;
            return -1 * i;
	}
        else{
	    x = valinfix0(expr, p);
	    op = expr[*p];
            if((op != '+') && (op != '-') && (op != '*') && (op != '/') && ERR == 0){
	            error(expr, *p, "数式が必要\n");
                return 0;
	    }
	    else{
                (*p)++;
                y = valinfix0(expr, p);
	    }
            (*p)++;
            if(op == '+'){
                answer = x + y;
            }
            if(op == '-'){
                answer = x - y;
            }
            if(op == '*'){
                answer = x * y;
            }
            if(op == '/'){
                answer = x / y;
            }
            return answer;
	    } //二項演算
    } //括弧で囲まれた式
    return 0;
}

//式の処理(インターフェース)
int valinfix(char *expr){
    int p, len1 = 0, len2 = 0, len3 = 0, place = 0;
    int i, j, k;
    int hirakikakko = 0;
    int tojikakko = 0;
    int count1 = 0, count2 = 0, count3 = 0, count4 = 0, SpaceCheck = 0;

    p = 0;
    ERR = 0;
  
    for(i = 0; expr[i] != '\n'; i++){
        if(expr[i] == '('){
            hirakikakko++;
        }
        if(expr[i] == ')'){
	        tojikakko++;
        }
        if(expr[i] == '(' && expr[i + 1] == ')'){
            for(j = 0; expr[j] != '\n'; j++){
                len2++;
                if(expr[j] == '(' && expr[j + 1] == ')'){
                    place = j;
                }
            }
            for(k = len2 + 1; k != place; k--){
                expr[k + 1] = expr[k];
            }
            expr[place + 1] = ' ';
            error(expr, i + 1, "括弧の中に数式が存在しません。\n");
            return 0;
        }
        if(expr[i] == ' '){
            error(expr, i, "空白(' ')を数式中に使用しないでください。\n");
	        SpaceCheck++;
	        if(SpaceCheck && expr[i] == '\n'){
                return 0;
            }
        }
        if(!isdigit(expr[i])){
            if((expr[i] != '+') && (expr[i] != '-') && (expr[i] != '*') && (expr[i] != '/')){
                if((expr[i] != '(') && expr[i] != ')' && expr[i] != ' '){
                    error(expr, i, "数字、演算子、'('、')'以外は入力しないでください。\n");
                }
            }
        }
        if(expr[0] == ')'){
            error(expr, 0, "式が正しくありません。\n");
            return 0;
        }
    }

    //printf("OK\n");
    //printf("%d,%d\n", hirakikakko, tojikakko);

    if(hirakikakko - tojikakko != 0){
        if(hirakikakko < tojikakko){
            for(i = 0; expr[i] != '\n'; i++){
                len1++;
            }
	        for(k = len1; k != 0; k--){
	            if(expr[k] == ')'){
		            count3 = k;
		            count4++;
		            if(hirakikakko < tojikakko){
		                hirakikakko++;
	                    error(expr, count3, "この閉じ括弧が余分です。\n");
                    }
	            } 
	        }   
        }   
    else if(hirakikakko > tojikakko){
	    for(j = 0; expr[j] != '\n'; j++){
	        if(expr[j] == '('){
		        count1 = j;
		        count2++;
	        }
	        if(hirakikakko > tojikakko){
                if(hirakikakko == 1 && tojikakko == 0){
                    printf("\nerror: 式が途中で終わっています。\n\n");
                    ERR = 1;
                    return 0;
                }
	            tojikakko++;
	                error(expr, count1, "この開き括弧が余分です。\n");
	        }
	    }
    }
    return 0;
    }
    if(hirakikakko == tojikakko){
        for(k = 0; expr[k] != '\n'; k++){
            len3++;
        }
        if(expr[len3 - 1] != ')'){
            if(hirakikakko > 0){
                printf("\nerror: 式が'()'で囲まれていません。\n\n");
                ERR = 1;
                return 0;
            }
        }
        if(expr[0] != '('){
            if(hirakikakko > 0 || tojikakko > 0){
                printf("\nerror: 式が'()'で囲まれていません。\n\n");
                ERR = 1;
                return 0;
            }
        }
        

        //return valinfix0(expr, &p);
    }
  return valinfix0(expr, &p);
}

//メイン関数
int main(){
    char expr[MAXLENGTH];
    int val;
    ERR = 0;

    fprintf(stderr, "Expression = ");
    while((fgets(expr, MAXLENGTH, stdin) != NULL) && (expr[0] != '\n')){
        val = valinfix(expr);
	    if(!ERR){
	        printf("Value = %d\n", val);
        }
        fprintf(stderr, "Expression = ");
    }
    return 0;
}


/*
1.中置記述式の構文

式 = 数 | "(" 単項演算子　式 ")" | "(" 式　2項演算子　式 ")".
数 = 数字 | 数 数字.
数字 = "0" | "1" | "2" | "3" | "4" | "5" | "6" | "7" | "8" | "9" .
単項演算子 = "-".
2項演算子 = "+" | "-" | "*" | "/" .

2.中置記述式の意味
「式」の意味をval(式)で表し、以下のように定義する。

val(数字)　= 数字に対応する10進１桁の数
val(数 数字) = val(数) * 10 + val(数字)
val (“(” “-” 式 “)”)　=　-val (式)
val (“(” 式1 “+” 式2 “)”)　=　val (式1) + val (式2) 
val (“(” 式1 “-” 式2 “)”)　=　val (式1) - val (式2) 	
val (“(” 式1 “*” 式2 “)”)　=　val (式1) * val (式2)
val (“(” 式1 “/” 式2 “)”)　=　val (式1) / val (式2)
*/

/*
出力結果
shii:programming isshi$ ./a.out
Expression = ((((1+2)-(-(3+4)))/2)*3)
Value = 15
Expression = 123
Value = 123
Expression = (1+1

error: 式が途中で終わっています。

Expression = (1+1))
(1+1))

     ^
error: この閉じ括弧が余分です。

Expression = (1+1a)
(1+1a)

    ^
error: 数字、演算子、'('、')'以外は入力しないでください。

Expression = (1+1)+1

error: 式が'()'で囲まれていません。

Expression = 1+(1+1)

error: 式が'()'で囲まれていません。

Expression = (()+1)
(( )+1)

  ^
error: 括弧の中に数式が存在しません。

Expression = (1 + 1)
(1 + 1)

  ^
error: 空白(' ')を数式中に使用しないでください。

(1 + 1)

    ^
error: 空白(' ')を数式中に使用しないでください。

Expression = (1 + 1a)
(1 + 1a)

  ^
error: 空白(' ')を数式中に使用しないでください。

(1 + 1a)

    ^
error: 空白(' ')を数式中に使用しないでください。

(1 + 1a)

      ^
error: 数字、演算子、'('、')'以外は入力しないでください。

Expression = )(1+1)
)(1+1)

^
error: 式が正しくありません。

Expression = ((1+1)
((1+1)

^
error: この開き括弧が余分です。

Expression =
*/

/*
考察
　中置記法の式を入力すると、その値を計算して文法チェックをして式が正しければ値を出力するプログラムを作成した。
逆ポーランド記法ではwhile(1)などの無限ループを用いて関数を書いていったが、今回の中置記法では、
文法が再帰的に定義されているため、再帰を用いてプログラムを完成させた。
式が何も入力されない状態でEnterキーを入力した場合を終了条件とし、それまでは何回でも式の入力を受け付ける。
入力した式が正しければその式の答えを、式に間違いがある場合は(可能な場合はエラーの場所を指摘して)エラーを表示し
新たな式を受け付ける状態に戻る。

木構造の時のような再帰を多用するようなプログラムではなかったので、そこまで苦労することなく正しい式の値を計算する
プログラムを書くことはできた。
しかし、問題はエラーの検出で、特に苦労したのは()のなかに数式が存在しない場合に空白を開けてエラー場所を指定するような
エラー処理を書く時だった。１マス分の空白を開けるときに、同時に空白検知のエラーが作動してしまったり、文字化けが発生したり
して、修正するまでに時間がかかった。
前回作った逆ポーランド記法のプログラムと組み合わせて、逆ポーランド記法を入力すると、一度中置記法を表示しその両方のエラー
検出をし、正しければ式の答え、間違いがあればエラーの指摘。というようなプログラムを作れば面白そうだなと思った。
まだまだ改良の余地があるプログラムなので、個人的に時間を使ってもっと良いものにしたいなと思った。
*/