#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>

#define MAXLENGTH 100

int ERR = 0; //エラー有りで1にセット

void error(char *expr, int p, char *message){
    int i;

    fprintf(stderr, "%s\n", expr); //式を表示
    for(i = 0; i < p; i++){ //エラーが見つかった位置を表示
        putc(' ', stderr);
    }
    putc('^', stderr);
    putc('\n', stderr);
    fprintf(stderr, "error: %s\n", message); //エラーメッセージを表示
    ERR = 1; // エラーフラグをセット
}

//数字1文字を対応する0~9の数に変換
int num1(char n){
    return (n - '0');
}

//数の処理
//isdigit : 10進数なら真
int number(char *expr, int *p){
    int value = 0;

    if(!isdigit(expr[*p])){
		error(expr,*p,"数字がありません。");
        return 0;
	}
    else{
        value = num1(expr[*p]);
        (*p)++;
         while(isdigit(expr[*p])){
	        value = value * 10 + num1(expr[*p]);
	        (*p)++;
        }
    }
    return value;
}

//式の処理(再帰)
int valinfix0(char *expr, int *p){
    int x, y, i, answer = 0; //部分式の評価結果
    char op; //演算子

    if(expr[*p] != '('){
        return number(expr, p);
    }
    else{
        (*p)++;
	    if(expr[*p] == '-'){
            (*p)++;
            i = valinfix0(expr, p);
            (*p)++;
            return -1 * i;
	    }
        else{
	        x = valinfix0(expr, p);
	        op = expr[*p];
	        if((op != '+') && (op != '-') && (op != '*') && (op != '/')){
	            error(expr, *p, "演算子が必要\n");
	        }
	        else{
                if(expr[*p] == ')'){
                    error(expr, *p, "不要な括弧があります。\n");
                }
                (*p)++;
                y = valinfix0(expr, p);
	        }
            (*p)++;
            if(op == '+'){
                answer = x + y;
            }
            if(op == '-'){
                answer = x - y;
            }
            if(op == '*'){
                answer = x * y;
            }
            if(op == '/'){
                answer = x / y;
            }
            return answer;
	    } //二項演算
    } //括弧で囲まれた式
    return 0;
}

//式の処理(インターフェース)
int valinfix(char *expr){
    int p, q;

    p = 0;
    q = valinfix0(expr, &p);
    if(expr[p] == '\n'){
        return q;
    }
    else{
        if(ERR == 0){
		    error(expr,p,"式が正しくありません\n");
        }
	}
    return 0;
}

//メイン関数
int main(){
    char expr[MAXLENGTH];
    int val;
    ERR = 0;

    fprintf(stderr, "Expression = ");
    while((fgets(expr, MAXLENGTH, stdin) != NULL) && (expr[0] != '\n')){
        val = valinfix(expr);
	    if(!ERR){
	        printf("Value = %d\n", val);
        }
        fprintf(stderr, "Expression = ");
    }
    return 0;
}