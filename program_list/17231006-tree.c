/*
[課題]
中置記法の式を読み込んで，木構造表現になおし，その節点を preorder, inorder, postorder の順でリストするプログラムを作成せよ．
リストに適当な句切りや括弧を入れて，それぞれ前置記法，中置記法，後置（逆ポーランド）記法になるようにせよ．
*/

#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>

//#define free(A) printf("free = %p\n", A)
#include "mem_util.h"

int ERR = 0; //エラー有りで1にセット
int first = 0;//計算式の最初を検知
#define MAXLENGTH 100

//木構造の構造体を定義
typedef struct _node{
    enum{LEAF, INTERNAL} kind;
    int value;
    struct _node *leftchild, *rightchild;
    char operator;
} node, *tree;

//LEAFの生成
tree create_leaf(int v){
    tree t;
    t = (tree)malloc(sizeof(node));
    t->kind = LEAF;
    t->value = v;
    return t;
}

//INTERNALの生成
tree create_internal(char op, tree sub1, tree sub2){
    tree t;
    t = (tree)malloc(sizeof(node));
    t->kind = INTERNAL;
    t->operator = op;
    t->leftchild = sub1;
    t->rightchild = sub2;
    return t;
}

//ノードの解放
void free_tree(tree t){
    if(t != NULL){
        if(t->kind == LEAF){
            free(t);
        }
        else{
            free_tree(t->leftchild);
            free_tree(t->rightchild);
            free(t);
        }
    }
}

//エラー検出時に警告文およびERR = 1にする
//エラーの場所を示すことが可能な場合は示す
void error(char *expr, int p, char *message){
    int i;

    fprintf(stderr, "%s\n", expr); //式を表示
    for(i = 0; i < p; i++){ //エラーが見つかった位置を表示
        putc(' ', stderr);
    }
    putc('^', stderr);
    putc('\n', stderr);
    fprintf(stderr, "error: %s\n", message); //エラーメッセージを表示
    ERR = 1; // エラーフラグをセット
}

//数字1文字を対応する0~9の数に変換
int num1(char n){
    return (n - '0');
}

//数の処理
//isdigit : 10進数なら真
tree number(char *expr, int *p){
    int value = 0;

    if(!isdigit(expr[*p]) && ERR == 0){
        if(expr[0] == '-'){
            error(expr, 0, "式が'()'で囲まれていません。");
            return 0;
        }
        error(expr,*p,"数字がありません。");
        return 0;
    }
    else{
        value = num1(expr[*p]);
        (*p)++;
        while(isdigit(expr[*p])){
	        value = value * 10 + num1(expr[*p]);
	        (*p)++;
        }
    }
    return create_leaf(value);
}

//式の処理(再帰)
tree infix_to_tree0(char *expr, int *p){
    //int x, y, i,  answer = 0; //部分式の評価結果
    char op; //演算子
    tree x, y, answer = NULL;
    
    if(expr[*p] != '('){
        return number(expr, p);
    }
    else{
        (*p)++;
	    if(expr[*p] == '-'){
            (*p)++;
            answer = create_internal('m', infix_to_tree0(expr, p), NULL);
            (*p)++;
            return answer;
	    }
        else{
	        x = infix_to_tree0(expr, p);
	        op = expr[*p];
            (*p)++;
            if((op != '+') && (op != '-') && (op != '*') && (op != '/') && ERR == 0){
	            error(expr, *p, "演算子の入力が不適。\n");
	        }
            else{
                if(expr[*p] == ')'){
                    error(expr,*p, "不要な括弧があります。\n");
                }
                y = infix_to_tree0(expr, p);
                answer = create_internal(op,x,y);
                (*p)++;
	        }
            return answer;
        }
        /*(*p)++;
        if(op == '+'){
        answer = x + y;
        }
        if(op == '-'){
            answer = x - y;
        }
        if(op == '*'){
            answer = x * y;
        }
        if(op == '/'){
            answer = x / y;
        }
        return answer;
	    } //二項演算*/
    } //括弧で囲まれた式
    return NULL;
}

//式の処理(インターフェース)
//主にエラー検出を行う
tree infix_to_tree(char *expr){
    int p, len1 = 0, len2 = 0, len3 = 0, place = 0;
    int i, j, k;
    int hirakikakko = 0;
    int tojikakko = 0;
    int count1 = 0, count2 = 0, count3 = 0, count4 = 0, SpaceCheck = 0;

    ERR = 0;
    p = 0;

    for(i = 0; expr[i] != '\n'; i++){
        if(expr[i] == '('){
            hirakikakko++;
        }
        if(expr[i] == ')'){
	        tojikakko++;
        }
        if(expr[i] == '(' && expr[i + 1] == ')'){
            for(j = 0; expr[j] != '\n'; j++){
                len2++;
                if(expr[j] == '(' && expr[j + 1] == ')'){
                    place = j;
                }
            }
            for(k = len2 + 1; k != place; k--){
                expr[k + 1] = expr[k];
            }
            expr[place + 1] = ' ';
            error(expr, i + 1, "括弧の中に数式が存在しません。\n");
            return NULL;
        }
        if(expr[i] == ' '){
            error(expr, i, "空白(' ')を数式中に使用しないでください。\n");
	        SpaceCheck++;
	        if(SpaceCheck && expr[i] == '\n'){
                return NULL;
            }
        }
        if(!isdigit(expr[i])){
            if((expr[i] != '+') && (expr[i] != '-') && (expr[i] != '*') && (expr[i] != '/')){
                if((expr[i] != '(') && expr[i] != ')' && expr[i] != ' '){
                    error(expr, i, "数字、演算子、'('、')'以外は入力しないでください。\n");
                }
            }
        }
        if(expr[0] == ')'){
            error(expr, 0, "式が正しくありません。\n");
            return NULL;
        }
    }

    if(hirakikakko - tojikakko != 0){
        if(hirakikakko < tojikakko){
            for(i = 0; expr[i] != '\n'; i++){
                len1++;
            }
	        for(k = len1; k != 0; k--){
	            if(expr[k] == ')'){
		            count3 = k;
		            count4++;
		            if(hirakikakko < tojikakko){
		                hirakikakko++;
	                    error(expr, count3, "この閉じ括弧が余分です。\n");
                    }
	            } 
	        }   
        }   
    else if(hirakikakko > tojikakko){
	    for(j = 0; expr[j] != '\n'; j++){
	        if(expr[j] == '('){
		        count1 = j;
		        count2++;
	        }
	        if(hirakikakko > tojikakko){
                if(hirakikakko == 1 && tojikakko == 0){
                    printf("\nerror: 式が途中で終わっています。\n\n");
                    ERR = 1;
                    return NULL;
                }
	            tojikakko++;
	                error(expr, count1, "この開き括弧が余分です。\n");
	        }
	    }
    }
    return NULL;
    }
    if(hirakikakko == tojikakko){
        for(k = 0; expr[k] != '\n'; k++){
            len3++;
        }
        if(expr[len3 - 1] != ')'){
            if(hirakikakko > 0){
                printf("\nerror: 式が'()'で囲まれていません。\n\n");
                ERR = 1;
                return NULL;
            }
        }
        if(expr[0] != '(' || expr[0] == '-'){
            if(hirakikakko > 0 || tojikakko > 0){
                printf("\nerror: 式が'()'で囲まれていません。\n\n");
                ERR = 1;
                return NULL;
            }
        }
    }
  return infix_to_tree0(expr,&p);
}

//前置記法
void preorder_list(tree t){
    if(t != NULL){
        if(t->kind == LEAF){
            if(first == 0){
                first = 1;
            }
            else{
                printf(",");
            }
            printf("%d", t->value);
        }
        else{
            if(first == 0){
                first = 1;
            }
            else{
                printf(",");
            }
            printf("%c", t->operator);
            preorder_list(t->leftchild);
            preorder_list(t->rightchild);
        }
    }
}

//中置記法
void inorder_list(tree t){    
    if(t != NULL){
        if(t->kind == LEAF){
            printf("%d", t->value);
        }
        else{
            printf("(");
            if(t->operator == 'm'){
                printf("-");
            }
            inorder_list(t->leftchild);
            if(t->operator != 'm'){ 
                printf("%c", t->operator);
                inorder_list(t->rightchild);
            }
        }
        if(t->kind == INTERNAL){
            printf(")");
        }
    }
}

//後置記法(逆ポーランド記法)
void postorder_list(tree t){
    if(t != NULL){
        if(t->kind == LEAF){
            if(first == 0){
                first = 1;
            }
            else{
                printf(",");
            }
            printf("%d", t->value);
        }
        else{
            postorder_list(t->leftchild);
            postorder_list(t->rightchild);
            if(first == 0){
                first = 1;
            }
            else{
                printf(",");
            }
            printf("%c", t->operator);
        }
    }
}

//メイン関数
int main(void){
    char expr[MAXLENGTH];
    tree val;
    ERR = 0;

    fprintf(stderr, "Expression = ");
    while((fgets(expr, MAXLENGTH, stdin) != NULL) && (expr[0] != '\n')){
        val = infix_to_tree(expr);
        if(!ERR){
            printf("前置記法: [ ");
            first = 0;
            preorder_list(val);
            printf(" ]\n");

            printf("中置記法: [ ");
            first = 0;
            inorder_list(val);
            printf(" ]\n");

            printf("後置記法: [ ");
            first = 0;
            postorder_list(val);
            printf(" ]\n");
            free_tree(val);
        }
        fprintf(stderr, "Expression = ");
    }
    return 0;
}

/*
[実行結果]
shii:programming isshi$ cc -Wall 17231006-tree.c
shii:programming isshi$ ./a.out
Expression = ((((1+2)-(-(3+4)))/2)*3)
[malloc(1):0x7fde0dd00008]
[malloc(2):0x7fde0de00008]
[malloc(3):0x7fde0de00038]
[malloc(4):0x7fde0de00068]
[malloc(5):0x7fde0de00098]
[malloc(6):0x7fde0de000c8]
[malloc(7):0x7fde0de000f8]
[malloc(8):0x7fde0de00128]
[malloc(9):0x7fde0de00158]
[malloc(10):0x7fde0de00188]
[malloc(11):0x7fde0de001b8]
[malloc(12):0x7fde0de001e8]
前置記法: [ *,/,-,+,1,2,m,+,3,4,2,3 ]
中置記法: [ ((((1+2)-(-(3+4)))/2)*3) ]
後置記法: [ 1,2,+,3,4,+,m,-,2,/,3,* ]
[free(11):0x7fde0dd00008]
[free(10):0x7fde0de00008]
[free(9):0x7fde0de00038]
[free(8):0x7fde0de00068]
[free(7):0x7fde0de00098]
[free(6):0x7fde0de000c8]
[free(5):0x7fde0de000f8]
[free(4):0x7fde0de00128]
[free(3):0x7fde0de00158]
[free(2):0x7fde0de00188]
[free(1):0x7fde0de001b8]
[free(0):0x7fde0de001e8]
Expression = ((1+1)
((1+1)

^
error: この開き括弧が余分です。

Expression = (1+1))
(1+1))

     ^
error: この閉じ括弧が余分です。

Expression = 123
[malloc(1):0x7fde0dc02758]
前置記法: [ 123 ]
中置記法: [ 123 ]
後置記法: [ 123 ]
[free(0):0x7fde0dc02758]
Expression = (1+1

error: 式が途中で終わっています。

Expression = (1+1a)
(1+1a)

    ^
error: 数字、演算子、'('、')'以外は入力しないでください。

[malloc(1):0x7fde0dc02758]
[malloc(2):0x7fde0dc02788]
[malloc(3):0x7fde0dc027b8]
Expression = (1 + 1a)
(1 + 1a)

  ^
error: 空白(' ')を数式中に使用しないでください。

(1 + 1a)

    ^
error: 空白(' ')を数式中に使用しないでください。

(1 + 1a)

      ^
error: 数字、演算子、'('、')'以外は入力しないでください。

[malloc(4):0x7fde0dd00008]
[malloc(5):0x7fde0dd00038]
[malloc(6):0x7fde0dd00068]
Expression = (1+1)+1

error: 式が'()'で囲まれていません。

Expression = 1+(1+1)

error: 式が'()'で囲まれていません。

Expression =
shii:programming isshi$
*/

/*
[考察]
中置記法の式を入力すると、木構造を用いることで前置記法、後置記法(逆ポーランド記法)に変換し出力するプログラムを作成した。
主に前回に作成した中置記法の演算をするプログラムを流用したが、大きく異なる点は木構造を導入したことだ。
しかし、プログラム全体で大きな変化はほとんどない。構造体を定義し、再帰を用いた程度である。
入力形式についても特に変わりはない。'Enterキー'を空入力で打つとプログラム終了、式の入力で式変換を行う。

また、入力は中置記法で行うので、前回作ったエラー処理は変数定義をtree型にして、'return'するものを少し変えれば応用可能である。
現にエラー処理もきちんと受け付けているので、プログラムが正常に動作しているとわかる。
orderの部分で、記法によって','を出力するもの、'(',')'を出力するものの場合分けや'm'への変換が少し難しかった。

次は一番重いと言われているソーティングの課題が待っているので、心して臨みたい。
*/