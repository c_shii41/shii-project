#include <stdio.h>

int main(void) {
    int n = 10;
    int input;
    int end = 100;
    int sum=0, i, j, k;

    scanf("%d", &input);

    while(1){
        k = (input / n);

        if(input < n){
            break;
        }

        j = input - n - 1;

        if(k >= 1 && k <= 10){
            sum = sum + j;
        } //入力数字が偶数桁 13/10 = 1.3
        else {
            sum = sum + input;
        }//入力数字が奇数桁　7/10=0.7

        n = n * 10;     
    }

    printf("%d\n", sum);
}