#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <unistd.h>
#include <netinet/in.h>

#define PORT        19991
#define MAX_CLIENT  5
#define FREE        100

int gcd(int x, int y) {
  int t;
  while (x > 0) {
    if (x < y) { t = x; x = y; y = t; }
    x = x - y;
  }
  return y;
}

main()
{
  int i, j, n, *server, *clients, sockets[MAX_CLIENT+2], len, x, y;
  fd_set fds;
  struct sockaddr_in saddr;
  struct sockaddr_in caddr;
  static char   buf[100];
  /* Intalize a socket ------------------------------------------ */
  server  = sockets;
  clients = sockets+1;
  if ((*server = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
    perror("socket");
    exit(1);
  }
  /* Bind the socket -------------------------------------------- */
  saddr.sin_family = AF_INET;
  saddr.sin_addr.s_addr = INADDR_ANY;
  saddr.sin_port = htons(PORT);
  if (bind(*server,(struct sockaddr *)&saddr,sizeof(struct sockaddr_in))<0) {
    perror("bind");  exit(1);
  }
  /* Wait to someone connect to this server --------------------- */
  if (listen(*server, MAX_CLIENT) < 0) {
    perror("listen"); exit(2);
  }
  /* Clear all clients ------------------------------------------ */
  for (i=0; i <= MAX_CLIENT; i++) clients[i]=FREE;
  /* Main loop -------------------------------------------------- */
  for (;;) {
    /* Initalize fd_set ----------------------------------------- */
    FD_ZERO(&fds);
    for (i=0; i < MAX_CLIENT+1; i++) {
      if (sockets[i] != FREE) FD_SET(sockets[i], &fds);
      printf("sokects[%d]=%d\n", i, sockets[i]);
    }
    /* Check message arrivals ----------------------------------- */
    if ((n = select(FD_SETSIZE, &fds, NULL, NULL, NULL)) == -1) {
       perror("select"); exit(3);
    } 
    /* Processing Loop ------------------------------------------ */
    for (i=0; i < MAX_CLIENT; i++) {
      if (clients[i] != FREE) {
	if (FD_ISSET(clients[i], &fds)) { /* A Message is exist */
	  if ((len = read(clients[i], buf, 100))==-1) {
	    perror("read"); exit(4);
	  } else if (len != 0) {
	    if (strncmp(buf, "quit", 4) != 0) { 
	      printf("A message from a client (%d) is arrived.\n", i);
	      sscanf(buf, "%d %d", &x, &y); 
	      sprintf(buf, "gcd(%d,%d)=%d\n", x, y, gcd(x,y));
	      write(clients[i], buf, 100);
	      bzero(buf, 100);
	    } else {
	      printf("A client (%d) leaved.\n", i);
	      close(clients[i]); clients[i] = FREE;
	    }
	  } else {
	    close(clients[i]); clients[i] = FREE;
	    printf("A client (%d) leaved.\n", i);
	  }
	}
      }
    }
    /* New connection -------------------------------------------*/
    if (FD_ISSET(*server, &fds) != 0) {
      len = sizeof(caddr);
      for (i=0; i < MAX_CLIENT; i++) {
	if (clients[i] == FREE) break;
      }
      clients[i] = accept(*server, (struct sockaddr*)&caddr, &len);
      if (clients[i] == -1) {
	perror("accept"); exit(5);
      }
      if (i < MAX_CLIENT) {
	printf("A new client (%d) is accepted.\n", i);
      } else {
	printf("A client is refused.\n");
	write(clients[i], "Server is too busy.\n", 20);
	close(clients[i]);
	clients[i]=FREE;
      }
    }
  }
}
