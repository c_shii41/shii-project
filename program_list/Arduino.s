#include <WProgram.h>
	
int APin4 = 4;
int APin1 = 1;
int DPin6 = 6;

void setup(){
  delay(5000);
  //データ転送レート9600に設定
  Serial.begin(9600);
  //液晶の初期化
  Serial.write(0xFE);
  Serial.write(0x01);
  //Hello Worldと表示(起動成功画面)
  Serial.print("Hello World!");
  delay(3000);
  //液晶の初期化
  Serial.write(0xFE);
  Serial.write(0x01);
}

void loop(){
    
  while(1){
      int centimeter = (analogRead(APin4) / 2) * 2.54;
      int Array[40];
      int sum = 0;
      int ave, temp, in, old = 0;

      //配列に数値を代入
      for(int i = 0; i <= 39; i++){
          Array[i] = centimeter;
          delay(50);
      }
      
      //得た数値をソートする
      for(int i = 0; i < 39; i++){
          for(int j = 39; j < i; j--){
              if(Array[j - 1] > Array[j]){
                  temp = Array[j - 1];
                  Array[j - 1] = Array[j];
                  Array[j] = temp;
              }
          }
      }
    
      //信頼性がそこそこ高い10~30番目のデータを使う
      for(int i = 10; i <= 29; i++){
      sum = sum + Array[i];
      }
  
      //平均をとる
      ave = sum / 20;

      while(1){
          Serial.print(ave);
          Serial.print("cm");
          delay(10);
          in = digitalRead(DPin6);
          if(in && !old){
              Serial.write(0xFE);
              Serial.write(0x01);
              break;
          }
          old = in;
        }
      continue;
    }
}

int main(void){
  init();

  setup();

  loop();

  return 0;
}


/*
実験器具の測定値の信頼性を上げるために
１　20個程度値をとり、上下5個の値を削る
２　音波を当てる素材（板、プラスチック、ガラス、スポンジ等）
３　正規分布などを用いることは時間コストがかかりすぎる
４　サランラップの芯を使うことで音波を正面に飛ばす
 */
