/*プログラミングR14 */
/* Created by 石田日々輝 on 2017/06/21 */
#include <stdio.h>
#include <math.h>
int main (void)
{
    int i,j,k;
    double a,b,c,d,θ[10],x[10],y[10],tempx,tempy,tempθ;

    a=0;
    b=0;
    printf("無理数座標を入力するなら'1'を、しないのであれば'0'を入力してください\n");
    for(i=0;i<10;i++){
        printf("無理数座標を入力しますか ( 1 or 0 )"); scanf("%d",&k);
/* 無理数座標を入力するなら"1"を、しないのであれば"0"を入力 */
        if(k==1){
            printf("x[%d]=",i); scanf("%lf",&a);
            printf("y[%d]=",i); scanf("%lf",&b);
/* √の中身が負の場合、エラーとなってしまうので場合分け */
            if(a<0){
                c=sqrt(fabs(a));
                x[i]=-c;
            }
            if(b<0){
                d=sqrt(fabs(b));
                y[i]=-d;
            }
            if(a>=0)
                x[i]=sqrt(a);
            if(b>=0)
                y[i]=sqrt(b);
        }
        if(k==0){
            printf("x[%d]=",i); scanf("%lf",&x[i]);
            printf("y[%d]=",i); scanf("%lf",&y[i]);
        }
/* 数学関数atan2を用いて角度を出す */
        θ[i]=atan2(y[i],x[i])*180/M_PI;
        if(θ[i]<0){
/* 角度が負になってしまうときは360を足して正の値に戻す */
        θ[i]=θ[i]+360;
        }
    }
/* θ,x,yの順に昇順でソートする */
    for(i=0;i<9;i++){
        for(j=i+1;j<10;j++){
			    if(θ[i]<θ[j]){
				    tempθ=θ[i];
				    θ[i]=θ[j];
				    θ[j]=tempθ;
                    tempx=x[i];
				    x[i]=x[j];
				    x[j]=tempx;
                    tempy=y[i];
                    y[i]=y[j];
                    y[j]=tempy;
                }
            }
    }
    for(i=0;i<10;i++){
		printf("θ=%lf\n",θ[i]);
        printf(" (x,y)=(%6.3lf,%6.3lf)\n",x[i],y[i]);
    }
    return 0;
}
