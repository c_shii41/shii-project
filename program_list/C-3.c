#include <WProgram.h>

//全て出力に設定
//※実際にanalogWriteによって使うDPinは3、5、6
void setup(){
  int PD;
  for(PD = 0; PD < 8; PD++){
    pinMode(PD, OUTPUT);
  }
  digitalWrite(0, 0);
}

//Light-Up&Down関数
void lightUD(){
  int i;
  int PD;
  
  //analogWriteで光量調節
  //Light-Up
  for(i = 0; i < 255; i++){
    for(PD = 0; PD < 8; PD++){
      if(PD == 3 || PD == 5 || PD == 6){
          analogWrite(PD, i);
      }
    }
    //255 * 5 でおおよそ1秒
    delay(5);
  }
  //Light-Down
  for(i = 255; i > 0; i--){
    for(PD = 0; PD < 8; PD++){
      if(PD == 3 || PD == 5 || PD == 6){
          analogWrite(PD, i);
      }
    }
    //255 * 5 でおおよそ1秒
    delay(5);
  }
}

int main(void){

  init();

  setup();
  //無限ループ
  while(1){
    lightUD();
  }

  return 0;
}
  

   
