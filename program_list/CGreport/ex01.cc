//
// ex01.cc - [演習1] プログラムファイル
//	作者: 石田　日々輝; 日付: 2019/01/07
// [注意] 以降の演習では，上記を適切に書き換えること
//

#include "rt01.h"
#include <iostream>
using namespace std;

//
// RTHit::intensity() - ピクセルの輝度(0〜255)を返す
//
unsigned char
RTHit::intensity(const RTLSrc& ls)
{
  double I = obj->g_kd() * ls.g_it() * nor.dotprod(ls.g_dir());

  if(I > 255){
    I = 255;
  }
  else if(I < 0){
    I = 0;
  }
  return I ; 
}

//
// main() - [演習1] メインルーチン
//
int
main()
{
        RTPoint vp, vrp, cnt;
	RTVec dir;
	double angle, rad, kd, it;
	int ds;

// ビューの設定 (標準入力からデータ入力)
	cin >> vp.c[0] >> vp.c[1] >> vp.c[2];	// 視点の入力
	cin >> vrp.c[0] >> vrp.c[1] >> vrp.c[2]; // 参照視点の入力
	cin >> angle >> ds;	// 視野角とピクセル数(各半分)の入力
	RTViewMgr view(vp, vrp, angle, ds);
// 物体(球)データの設定 (標準入力からデータ入力)
	cin >> cnt.c[0] >> cnt.c[1] >> cnt.c[2]; // 球の中心の入力
	cin >> rad;                              // 球の半径の入力
	cin >> kd;                               // 拡散反射率の入力
	RTObject obj(cnt, rad, kd);
//拡散反射率の設定
	cin >> dir.c[0] >> dir.c[1] >> dir.c[2];
	cin >> it;
	RTLSrc line(dir, it);
// 各ピクセルの輝度の計算と標準出力への出力
	view.prnpgmhdr();	// PGMファイルのヘッダ出力
	RTRay ray;
	RTHit hit;
	for(int y = 2 * ds - 1; y >= 0; y--)
		for(int x = 0; x < 2 * ds; x++) {
			view.getray(x, y, &ray); // ピクセルへの光線を得る
			if(obj.rayhit(ray, &hit))
				cout << hit.intensity(line);
			else
				cout << static_cast<unsigned char>(0);
		}
}
