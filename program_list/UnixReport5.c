/* Report4.c */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <string.h>


#define CHOICEBUFSIZ 128 //選択肢入力用の文字列バッファ
#define DIRBUFSIZ 256
#define CHARBUFSIZ 128

int main(void) {
  char chbuf[CHOICEBUFSIZ]; //選択肢入力用バッファ
  char charbuf[CHARBUFSIZ];
  int choice; //選択肢番号
  int status; //子プロセスの終了状態 : 使用しない
  char dirbuf[DIRBUFSIZ]; //ディレクトリ名のバッファ

// printcwd() カレントワーキングディレクトリの表示
void printcwd(void) { 
  if(getcwd(dirbuf, DIRBUFSIZ) == NULL) {
    fprintf(stderr, "getcwd() error\n");
    exit(1);
  }
  printf("pwd = %s\n", dirbuf);
}

  while(1) {
    printf("\n");
    printf("1. cal\n");
    printf("2. ls\n");
    printf("3. ps\n");
    printf("4. pwd\n");
    printf("5. cd\n");
    printf("9. exit\n");
    printf("choose number : ");
    fgets(chbuf, CHOICEBUFSIZ, stdin);
    printf("\n");
    choice = atoi(chbuf);

    if (choice == 9) {
      exit(0);
    }

    if(choice == 4) {
      printcwd();
      continue;
    }

    if(choice == 5) {
      getcwd(dirbuf, DIRBUFSIZ);
      printf("cd ");
      fgets(charbuf, CHARBUFSIZ, stdin);
      //printf("%s\n", dirbuf);
      strcat(dirbuf, "/");
      strcat(dirbuf, charbuf);
      dirbuf[strlen(dirbuf) - 1] = '\0';
      //printf("%s\n", dirbuf);
      if(chdir(dirbuf)) {
	printf("\nchdir to '%s' failed\n", dirbuf);
      }
      else{
	printf("chdir to %s succeeded\n", dirbuf);
      }
      continue;
    }

    if(fork() == 0){
      switch(choice) {
      case 1 :
	execl("/usr/bin/cal", "cal", NULL);
	break; //exec() 失敗用
      case 2 :
	execl("/bin/ls", "ls", NULL);
	break; //exec() 失敗用
      case 3 :
	execl("/bin/ps", "ps", NULL);
	break; //exec() 失敗用
      }
      fprintf(stderr, "cannot execute command\n");
      exit(1); //exec() 失敗時はすぐに終了する
    }
    else {
      wait(&status);
    }
  }
  return 0;
}
