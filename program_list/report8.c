/**********************************************************************/
/* 配列を用いたスタックの実現                                              */
/**********************************************************************/

#include <stdio.h>  /* getchar(), putchar(), printf() */
#include <stdlib.h> /* exit() */
#define STACKSIZE 100 /* スタックサイズ */

typedef struct{
    char kind;
    int line;
    int pos;
} elem_t;

typedef struct{
    elem_t data[STACKSIZE]; /* データ格納領域 */
    int top;                /* スタックトップの添字位置 */
} st_t;                     /* スタック型     */

/**********************************************************************/
/* 関数のプロトタイプ宣言                                                 */
/**********************************************************************/

void   initStack(st_t *st);
void   push(elem_t x, st_t *st);
elem_t pop(st_t *st);
elem_t peekStack(const st_t *st);
int    isEmptyStack(const st_t *st);
int    isFullStack(const st_t *st);
void   printStack(const st_t *st);

/**********************************************************************/
/* スタック操作関数定義                                                  */
/**********************************************************************/

// 空スタックの作成
void initStack(st_t *st){
    st->top = -1; /* 空のスタックの top は -1 とする */
}

elem_t peekStack(const st_t *st){
    if (isEmptyStack(st)){
        printf("PeekStack Error: Stack Empty\n");
        exit(1);
    }
    return st->data[st->top];
}

// プッシュする
void push(elem_t x, st_t *st){
    st->top++;
    st->data[st->top] = x;
}

// ポップする
elem_t pop(st_t *st){
    elem_t c;

    c = st->data[st->top];
    st->top--;

    return c;
}

// スタックが空かどうかのチェック
int isEmptyStack(const st_t *st){
    if (st->top == -1){
        return 1;
    }
    else
        return 0;
}

// スタックが満杯かどうかのチェック
int isFullStack(const st_t *st){
    if (st->top == STACKSIZE - 1){
        return 1;
    }
    else
        return 0;
}

// 括弧が非対応の時、その行と列を出力
void incomp(int a, int b, int c, int d){
    printf("%d行目%d列目の開き括弧と%d行目%d列目の閉じ括弧が非対応です。\n", a, b, c, d);
}

// 閉じ括弧が余分な時、出力
void closeover(int c, int d){
    printf("%d行目%d列目の閉じ括弧が余分です。\n", c, d);
}

/**********************************************************************/
/* メインプログラム                                                      */
/**********************************************************************/

int main(){
    char c;
    st_t st;
    int i, line = 1, pos = 1, count1 = 0, count2 = 0;
    int temp1 = 0, temp2 = 0, temp3 = 0, temp4 = 0;
    int temp5 = 0, temp6 = 0;
    elem_t checker, popchecker, bottomstack;

    initStack(&st); /* スタックの初期化 */
    printf("input line\n");

    while ((c = getchar()) != EOF){ /* 改行になるまで一文字ずつ読込んで*/
        if (c == '\n'){ /* 改行したとき、行は+1、列は初期化 */
            line++;
            pos = 1;
            continue;
        }
        if (c == '(' || c == '['){ /* 開き括弧の時の動作 */
            checker.kind = c;
            checker.line = line;
            checker.pos = pos;
            push(checker, &st); /* 文字をスタックにプッシュ */
        }
        if (c == ')' || c == ']'){ /* 閉じ括弧の時の動作 */
            if (isEmptyStack(&st) == 1){ /* スタックが空の時、カウンタ変数を+1 */
                count2++;                /* 括弧の対応が違う時、カウンタ変数を+1 */
                temp5 = line;           /* 各値をtempに避難させる */
                temp6 = pos;
                pos++;
                continue; /* 動作は続行する */
            }
            popchecker = pop(&st); /* スタックからポップして前の括弧のデータを読み込む */
            if (popchecker.kind == '(' && c == ')'){
                pos++;
                continue;
            }
            if (popchecker.kind == '[' && c == ']'){
                pos++;
                continue; /* 上と下の二つは統合可能 */
            }
            if ((popchecker.kind == '(' && c == ']') || (popchecker.kind == '[' && c == ')')){
                count1++;
                temp1 = popchecker.line;
                temp2 = popchecker.pos; /* 括弧の対応が違う時、カウンタ変数を+1 */
                temp3 = line;           /* 各値をtempに避難させる */
                temp4 = pos;
                pos++;
                continue; /* 動作は続行する */
            }
        }
        pos++;
    }
    // count1を通ったらincompへ行って終了
    if (count1){
        incomp(temp1, temp2, temp3, temp4);
        return 0;
    }
    // count2を通ったらcloseoverへ行って終了
    if (count2){
        closeover(temp5, temp6);
        return 0;
    }
    // エラーがないとき
    if (isEmptyStack(&st) == 1 && c == EOF){
        printf("エラーはありません。\n");
        return 0;
    }
    // いずれの条件にも当てはまらない時は開き括弧が余分になっている
    // スタックの一番下にその位置情報が保存されているので、popすることにより無理やり呼び出す
    bottomstack = pop(&st);
    printf("%d行目%d列目の開き括弧が余分です。\n", bottomstack.line, bottomstack.pos);

    return 0;
}