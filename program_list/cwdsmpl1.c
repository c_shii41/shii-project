/* cwdsmpl1.c */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define DIRBUFSIZ 128 /* ディレクトリ名のバッファサイズ */

/* printcwd() カレントワーキングディレクトリの表示 */
void printcwd(void) {
    char dirbuf[DIRBUFSIZ]; /* ディレクトリ名のバッファ */
    
    if(getcwd(dirbuf, DIRBUFSIZ) == NULL) {
        fprintf(stderr, "getcwd() error\n");
        exit(1);
    }
    printf("cwd = %s\n", dirbuf);
}

int main(int argc, char* argv[]) {
    printcwd();
    /* カレントワーキングディレクトリの移動 */

    if(argc > 1) {
        if(chdir(argv[1])){
            printf("chdir to %s failed\n", argv[1]);
        }
        else{
            printf("chdir to %s succeeded\n", argv[1]);
        }
        printcwd();
        return 0;
    }
}

