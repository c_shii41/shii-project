import socket
import threading
import re
import sys
import time

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# 接続先
host = "0.0.0.0"
port = 55565
sock.connect((host, port))
first = 0
end_socket = 0

def Handler(sock):
    while True:
        try:
            #name = sock.recv(4096)
            res = sock.recv(4096)
            print(res.decode())
        except Exception as e:
            print(e)
            break

if __name__ == '__main__' :
    while True:
        if first == 0 :
            your_name = input("name : ")
            sock.send(your_name.encode("UTF-8"))
            first = 1
            print("")
            print("--- Chat Start ---")
        your_input = input()
        sock.send(your_input.encode("UTF-8"))
        if your_input == 'logout' :
            break
            #time.sleep(0.5)
        thread = threading.Thread(target = Handler, args = (sock,), daemon = True)
        thread.start()
    sock.shutdown(socket.SHUT_RDWR)
    sock.close()
    #sys.exit()