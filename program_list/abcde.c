#include<stdio.h>
#include<string.h>
#include<ctype.h>
#include<stdlib.h>


#define MAXLENGTH 200
int ERR = 0; //エラーフラグ

//文字で与えられた数値をint型に変換する関数。
int num1(char n)
{
	return atoi(&n); //return n - '0' でも変換可能
}


//エラーが起こったときに、エラーの内容とエラーが起こった場所を表示する関数
void error(char *expr, int p, char *message)
{
	fprintf(stderr,"%s\n",expr);

	for(int i = 0; i < p; i += 1)
	{
		putc(' ',stderr);
	}
	putc('^',stderr);
	putc('\n',stderr);

	fprintf(stderr,"error:%s\n",message);
	ERR = 1;
}

//文字変数で与えられた1桁以上の数値をint型に変換する関数
int number(char *expr, int *p)
{
	int value;

	if(! isdigit(expr[*p]))
	{
		error(expr,*p,"数字がありません。");
	}
	else
	{
		//printf("ad %c %c\n",expr[*p],expr[(*p)+1]);
		value = num1(expr[*p]);
		(*p) += 1;
		while(isdigit(expr[*p]))
		{
			value = value * 10 + num1(expr[*p]);
			(*p) += 1;
		}
	}
	return value;
}



//
int valinfix0(char *expr,int *p)
{
	int x,y,ans = 0,i; //部分式の評価結果
	char op; //演算子
	if(expr[*p] != '(')
	{
		return number(expr,p);
	}
	else
	{
		(*p) += 1;
		if(expr[*p] == '-')
		{
			(*p) += 1;
			i = valinfix0(expr,p);
			(*p) += 1;
			return -1 * i;
		}
		else
		{
			if(expr[0] != '(')
			{
				error(expr,0,"始点に(が入っています。");
				return 0;
			}
			//printf("%c %d TOCHU\n",expr[*p], *p);
			x = valinfix0(expr,p);
			op = expr[*p];
			if((op != '+') && (op != '-') && (op != '*') &&(op != '/'))
			{
				error(expr,*p,"演算子の入力方法を間違えています。\n");
			}
			else
			{
				if(expr[*p] == ')')
				{
					error(expr,*p,"不要な括弧が入っています。\n");
				}
				(*p) += 1;
				y = valinfix0(expr,p);
			}
			(*p) += 1;
			switch(op)
			{
				case '+':
					ans =  x + y;
					break;
				case '-':
					ans = x - y;
					break;
				case '*':
					ans = x * y;
					break;
				case '/':
					ans = x / y;
					break;
			}
			//printf("Now Moji: %c  No.%d\n",expr[*p],*p);
			//printf("kakkokosu %d\n",kakko);
				return ans;
		}
	}
	return 0;
}



int valinfix(char *expr)
{
	int dest,subtotal;

	dest = 0;
	ERR = 0;
	subtotal = valinfix0(expr,&dest);
	if(expr[dest] == '\n')
	{
		return subtotal;
	}
	else
	{
		error(expr,dest,"式が正しくありません\n");
	}

	return 0;
}

int main()
{
	char expr[MAXLENGTH];
	int val;
	fprintf(stderr,"Expression = ");

	while ((fgets(expr,MAXLENGTH,stdin) != NULL) && (expr[0] != '\n'))
	{
		val = valinfix(expr);
		if(! ERR)
		{
			printf("Value = %d\n",val);
		}
		fprintf(stderr,"Expression = ");
	}
	return 0;
}