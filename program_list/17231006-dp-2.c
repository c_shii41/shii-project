/*
[課題1]
関数 r_comb(n, m) を呼び出して計算するとき，r_comb は最初の呼び出しも含めて合計で何回呼び出されるか
（実際のプログラムで呼び出し回数を求めるようして，いろいろな場合を観察して予測をたてよ.
  その呼び出し回数の予測式が正しいことを漸化式で調べよ．）

[課題2]
動的計画法に基づいて nCm を求める関数 d_comb を書け．
また，この関数を計算する手間はどれくらいか，最悪の場合を考えて，n, m を用いた式で考えよ．オーダーで答えよ
※ 考察に用いたプログラムとともにレポートを作成せよ．また，r_combとd_combの実際の計算時間を測定した比較も行うこと．
*/
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <time.h>

#define Array_size 10000 //fgetsの配列確保
int r_count = 0;  //r_combの実行回数カウンタ
int d_count = 0;  //d_combの実行回数カウンタ

//r_comb
unsigned long long int r_comb(int n, int m){
  r_count++;
  if(m == 0 || m == n){
    return 1;
  }
  else{
    return r_comb(n - 1, m) + r_comb(n - 1,m - 1);
  }
}

//d_comb
unsigned long long int d_comb(int n, int m, unsigned long long int dp[n - m + 1][m + 1]){
  int i, j;

  for(i = 0; i <= n - m; i++){
    for(j = 0; j <= m; j++){
      if(i == 0 || j == 0){
        dp[i][j] = 1;
      }
      else{
        dp[i][j] = dp[i - 1][j] + dp[i][j - 1];
      }
      d_count++;
    }
  }
  return dp[n - m][m];
}

//数字に変換
int num1(char value){
  return atoi(&value);
}

//数字ならばvalueに格納
int number(char *expr, int *next){
  int value;
  
  if(!isdigit(expr[*next])){;
    exit(1);
  }
  else{
    value = num1(expr[*next]);
    (*next)++;
    while(isdigit(expr[*next])){
      value = value * 10 + num1(expr[*next]);
      (*next)++;
    }
  }
  (*next) = 0;
  return value;
}

int main(void){
  int n, m, temp, con = 0, D = 0, pos = 0;
  unsigned long long int ans, answer;
  char value[Array_size];
  clock_t start_time, end_time;
  double cpu_time;

  //無限ループ
  while(1){
    //変数初期化
    con = 0;
    r_count = 0;
    d_count = 0;
    D = 0;
    pos = 0;

    printf("n C m\n");
  
    printf("n = ");
    fgets(value, Array_size, stdin);
    n = number(value, &pos);

    printf("m = ");
    fgets(value, Array_size, stdin);
    m = number(value, &pos);
    
    if(n < m){
      printf("Error : n < m となる値は存在しません。\n");
      continue;
    }

    if((n / 2 != m) && ((n - m) <= n / 2)){
      temp = m;
      m = n - m;
      D++;
    }
    else{
      temp = m;
    }

    unsigned long long int dp[n - m + 1][m + 1];

    start_time = clock();
    answer = d_comb(n, m, dp);
    end_time = clock();
    cpu_time = (double)(end_time - start_time) / CLOCKS_PER_SEC * 100;
    printf("\n-----d_comb-----\n");
    if(D == 1){
      printf("%d C %d = ", n, temp);
    }
    printf("%d C %d = %llu\n", n, m, answer);
    printf("%8lf ms\n", cpu_time);
    printf("実行回数は%d回\n\n", d_count);

    //d_combの表示
    for(int i = 0; i <= (n - m); i++){
      for(int j = 0; j <= m; j++){
        //printf("\n%d, %d\n", i, j);
        printf("%8llu", dp[i][j]);
      }
      printf("\n");
    }
    
    start_time = clock();
    ans = r_comb(n, m);
    end_time = clock();
    cpu_time = (double)(end_time - start_time) / CLOCKS_PER_SEC * 100;
    printf("\n-----r_comb-----\n");
    if(D == 1){
      printf("%d C %d = ", n, temp);
    }
    printf("%d C %d = %llu\n", n, m, ans);
    printf("%8lf ms\n", cpu_time);
    printf("実行回数は%d回\n\n", r_count);

    //r_combの表示
    for(int i = 0; i < n; i++){
      for(int j = 0; j < temp && j <= i; j++){
        printf("%8llu", r_comb(i, j));
      }
      if(temp != 0){
        printf("\n");
      }
    }

    //続けて実行するか
    printf("\nContinue?  1 / 0\n");
    printf("1 -> yes / 0 -> no\n");
    
    fgets(value, Array_size, stdin);
    con = number(value, &pos);
    
    if(con == 1){
      printf("\n");
      continue;
    }
    else{
      printf("プログラムを終了します。\n");
      return 0;
    }
  }
  return 0;
}

/*実行結果
n C m
n = 5
m = 2

-----d_comb-----
5 C 2 = 10
0.000100 ms
実行回数は12回

       1       1       1
       1       2       3
       1       3       6
       1       4      10

-----r_comb-----
5 C 2 = 10
0.000100 ms
実行回数は19回

       1
       1       1
       1       2
       1       3
       1       4

Continue?  1 / 0
1 -> yes / 0 -> no
1

n C m
n = 6
m = 4

-----d_comb-----
6 C 4 = 6 C 2 = 15
0.000000 ms
実行回数は15回

       1       1       1
       1       2       3
       1       3       6
       1       4      10
       1       5      15

-----r_comb-----
6 C 4 = 6 C 2 = 15
0.000100 ms
実行回数は29回

       1
       1       1
       1       2       1
       1       3       3       1
       1       4       6       4
       1       5      10      10

Continue?  1 / 0
1 -> yes / 0 -> no
1

n C m
n = 10
m = 3

-----d_comb-----
10 C 3 = 120
0.000100 ms
実行回数は32回

       1       1       1       1
       1       2       3       4
       1       3       6      10
       1       4      10      20
       1       5      15      35
       1       6      21      56
       1       7      28      84
       1       8      36     120

-----r_comb-----
10 C 3 = 120
0.000200 ms
実行回数は239回

       1
       1       1
       1       2       1
       1       3       3
       1       4       6
       1       5      10
       1       6      15
       1       7      21
       1       8      28
       1       9      36

Continue?  1 / 0
1 -> yes / 0 -> no
1

n C m
n = 20
m = 10

-----d_comb-----
20 C 10 = 184756
0.000200 ms
実行回数は121回

       1       1       1       1       1       1       1       1       1       1       1
       1       2       3       4       5       6       7       8       9      10      11
       1       3       6      10      15      21      28      36      45      55      66
       1       4      10      20      35      56      84     120     165     220     286
       1       5      15      35      70     126     210     330     495     715    1001
       1       6      21      56     126     252     462     792    1287    2002    3003
       1       7      28      84     210     462     924    1716    3003    5005    8008
       1       8      36     120     330     792    1716    3432    6435   11440   19448
       1       9      45     165     495    1287    3003    6435   12870   24310   43758
       1      10      55     220     715    2002    5005   11440   24310   48620   92378
       1      11      66     286    1001    3003    8008   19448   43758   92378  184756

-----r_comb-----
20 C 10 = 184756
0.096200 ms
実行回数は369511回

       1
       1       1
       1       2       1
       1       3       3       1
       1       4       6       4       1
       1       5      10      10       5       1
       1       6      15      20      15       6       1
       1       7      21      35      35      21       7       1
       1       8      28      56      70      56      28       8       1
       1       9      36      84     126     126      84      36       9       1
       1      10      45     120     210     252     210     120      45      10
       1      11      55     165     330     462     462     330     165      55
       1      12      66     220     495     792     924     792     495     220
       1      13      78     286     715    1287    1716    1716    1287     715
       1      14      91     364    1001    2002    3003    3432    3003    2002
       1      15     105     455    1365    3003    5005    6435    6435    5005
       1      16     120     560    1820    4368    8008   11440   12870   11440
       1      17     136     680    2380    6188   12376   19448   24310   24310
       1      18     153     816    3060    8568   18564   31824   43758   48620
       1      19     171     969    3876   11628   27132   50388   75582   92378

Continue?  1 / 0
1 -> yes / 0 -> no
0
プログラムを終了します。
*/

/*課題1考察
  関数r_combをできる限り多く実行し、引数に対応するnCmの実行回数の値を求めた結果として以下を得た。
  0 C 0 -> 1回
  1 C 0 -> 1回    1 C 1 -> 1回
  2 C 0 -> 1回    2 C 1 -> 3回    2 C 2 -> 1回
  3 C 0 -> 1回    3 C 1 -> 5回    3 C 2 -> 5回    3 C 3 -> 1回
  4 C 0 -> 1回    4 C 1 -> 7回    4 C 2 -> 11回   4 C 3 -> 7回    4 C 4 -> 1回
  5 C 0 -> 1回    5 C 1 -> 9回    5 C 2 -> 19回   5 C 3 -> 19回   5 C 4 -> 9回    5 C 5 -> 1回
  6 C 0 -> 1回    6 C 1 -> 11回   6 C 2 -> 29回   6 C 3 -> 39回   6 C 4 -> 29回   6 C 5 -> 11回   6 C 6 -> 1回

  以上の結果から、nCmの実行回数を(2 * nCm - 1)と予測した。
  数学的帰納法よりこの予測が正しいか示す。
  証)
    nCmの実行回数をnGmと定義する。
    上記の結果から、nGm = n-1Gm + n-1Gm-1 + 1が成り立つ。(n > 1, m > 0, n != m)

    1)n = 2, m = 1のとき
      2G1 = 1G1 + 1G0 + 1
          = 1 + 1 + 1
          = 3
    よって成立。

    2)n = s, m = tのとき
      sGt = s-1Gt + s-1Gt-1 + 1
          = (2 * s-1Ct - 1) + (2 * s-1Ct-1 - 1) + 1
          = 2(s-1Ct + s-1Ct-1) - 1
          = 2{(s - t) * (s - 1)! / (t!(s - t)!) + t * (s - 1)! / (t!(s - t)!)} - 1
          = 2{((s - t) + t) * (s - 1)! / t!(s - t)!} - 1
          = 2{s! / (t!(s - t)!)} - 1
          = 2 * sCt - 1;

      n = s + 1, m = t + 1のとき
      s+1Gt+1 = sGt+1 + sGt + 1
          = 2(sCt+1 + sCt) - 1
          = 2{((t + 1) * s! + (s - t) * s!) / (t + 1)! * (s - t)!}
          = 2{(s + 1)! / (t + 1)! * (s - t)!} - 1
          = 2 * s+1Ct+1 - 1
      従ってn = s + 1, m = t + 1の時も成立。

      以上のことから、nCmの実行回数は(2 * nCm - 1)である。。
  */

 /*課題2考察
  この関数を計算する手間は実行結果を見るとわかるように、 20C10の時は121回、10C3の時は32回である。
  今回書いたd_combは解が２次元配列の[n][m]番目に格納してあるので、最悪の場合のオーダーOはO((n - m + 1)^2)である。

  ●測定時間についての考察
  動的計画法を用いたd_combでは、工夫なしの再帰を用いたr_combよりも圧倒的に速く答えを割り出すことができる結果になった。
  今回のd_combでは再帰処理をするために関数を再度呼び出す時間が惜しいを思い、再帰を用いずにfor文のみで書いた。
  結果はまさに雲泥の差と言っていいほどの差が出ており、動的計画法の重要性を感じることができた。
 */