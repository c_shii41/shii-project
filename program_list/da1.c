/**************************/
/* 演習のプログラムの概形 */
/**************************/

#include <stdio.h>

/**********************************************************************/
/* 構造体の宣言                                                       */
/**********************************************************************/
/* ベクトルを表わす構造体 */
typedef struct vector {
   double x;   /* x 成分 */
   double y;   /* y 成分 */
} vec_t;

/**********************************************************************/
/* 関数の定義                                                         */
/**********************************************************************/
/* ベクトルの値の設定 その1 */
void vSet(double x, double y, vec_t *v) {
   v->x = x;   /* ポインタ v を介した構造体メンバ x の指定 : v->x */
   v->y = y;
   /* y 成分についても同様にする */
}

/* ベクトルの値の設定　その2 */
vec_t vCon(double x, double y){
    vec_t v;

    v.x = x;
    v.y = y;

    return v;
}

/* ベクトルの和を計算して返す */
vec_t vAdd(vec_t va, vec_t vb) {
    vec_t v;

    v.x = va.x + vb.x;
    v.y = va.y + vb.y;

    return v;
}

/* ベクトルの差を計算して返す */
vec_t vSub(vec_t va, vec_t vb) {
    vec_t v;

    v.x = va.x - vb.x;
    v.y = va.y - vb.y;

    return v;
}

/* ベクトルのスカラー倍を計算して返す */
vec_t vMul(double k, vec_t va) {
    vec_t v;

    v.x = va.x * k;
    v.y = va.y * k;

    return v;
}

double vProd(vec_t va, vec_t vb) {
    return (va.x * vb.x + va.y * vb.y);
}

/* ベクトルの表示 */
void vPrint(vec_t V) {
    printf("(%lf,%lf)\n",V.x,V.y);
   /* va の x 成分と y 成分 を  */
   /* printf を使ってベクトルらしく表示する  */
}

/**********************************************************************/
/* メインプログラム                                                   */
/**********************************************************************/
int main(void) {
   vec_t a, b, c, d;
   double k = 7;

   vSet(5, 4, &a);  /* 方法1 */
   printf("ベクトル a = "); vPrint(a);

   b = vCon(-2, 9);  /* 方法2 */
   printf("ベクトル b = "); vPrint(b);

   printf("スカラー k = %f\n\n", k);

   printf("a + b = "); vPrint(vAdd(a, b));  /* ベクトルの和 */
   printf("a - b = "); vPrint(vSub(a, b));  /* ベクトルの差 */
   printf("k * a = "); vPrint(vMul(k, a));  /* スカラ倍 */
   printf("a . b = %f\n", vProd(a, b));     /* 内積 */
   printf("a + (1, 6) = "); vPrint(vAdd(a, vCon(1, 6)));


   printf("\n原点とベクトル a, b で作られる三角形を考える\n");
   c = vSub(a, b);
   printf("c = a - b とすると，c = "); vPrint(c);

   printf("余弦定理が成立することを計算して確かめる\n");
   printf("余弦定理： |c|^2 = |a|^2 + |b|^2 - 2 * a.b\n");
   printf("   ここで a.b は「a と b の内積」のこと\n\n");

   printf("|c|^2    = %f\n", vProd(c, c));
   printf("|a|^2    = %f\n", vProd(a, a));
   printf("|b|^2    = %f\n", vProd(b, b));
   printf("2*a.b    = %f\n", 2*vProd(a, b));
   printf("右辺の値 = %f\n\n", vProd(a, a) + vProd(b, b) - 2*vProd(a, b));

   /* 「a から b を引いたベクトル」に「a と b の内積」を掛けてできた  */
   /* ベクトルに (9, 6) と成分表示されるベクトルを加えたものを        */
   /* d に代入する．以下の ??????? の部分を埋めて処理を完成させよ     */

   d = vAdd(vMul(vProd(a,b),vSub(a,b)), vCon(9,6));

   printf("d = (a . b) * (a - b) + (9, 6) = "); vPrint(d);

   return 0;
}