import socket
import concurrent.futures
import sys

# ソケット初期化
host = '0.0.0.0'
port = 61114
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
try:
    s.connect((host, port))
except ConnectionRefusedError as e:
    print('サーバー側から接続拒否されました．')
    exit(1)
except Exception as e:
    print(e)
    exit(1)

continue_flag = True

def recv() :
    while True :
        try :
            res = s.recv(4096)
            if res.decode() == "*logout" :
                print(">> 正常終了しました。")
                continue_flag = False
                return
            print(res.decode())
        except Exception as error :
            print(error)
            return

def send() :
    print("何も入力せずにEnterキーを押すか、*logoutと入力すると終了します。")
    print("ログイン中のユーザーを一覧したい場合は*nameを入力してください。")
    while True :
        your_input = input(">> ")
        try :
            #s.send(your_input.encode("UTF-8"))
            if not your_input or your_input == "*logout" :
                s.send("*logout".encode("UTF-8"))
                print(">> ログアウトします...")
                continue_flag = False
                return
            elif your_input == "*name" :
                s.send("*name".encode("UTF-8"))
                print(">> 現在ログインしているユーザーを表示します。")
            else :
                s.send(your_input.encode("UTF-8"))
        except Exception as error :
            print(error)
            return

# Main Function
if __name__ == "__main__" :
    try :
        print("Wait...")
        print(s.recv(4096).decode())
        your_name = input("ユーザーネーム >> ")
        if not your_name :
            your_name = "名無しソケットさん"
        s.send(your_name.encode("UTF-8"))
        start_frag = True
    except Exception as error :
        print(error)
        error_flag = True

    executor = concurrent.futures.ThreadPoolExecutor(max_workers = 2)
    try :
        if continue_flag == True :
            executor.submit(recv)
        if continue_flag == True :
            executor.submit(send)
    except Exception as error :
        print(error)