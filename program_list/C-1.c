#include <WProgram.h>

//0~7番を出力に設定
void setup(){
  int PD;
  for(PD = 0; PD < 8; PD++){
    pinMode(PD, OUTPUT);
  }
}

//光点が1つずつ増えていく関数
void light(){
  int PD;
  //0~7番を1秒おきに光点を1つ増やしながら点灯
  for(PD = 0; PD < 8; PD++){
    digitalWrite(PD, HIGH);
    delay(1000);
  }
  //全消灯
  for(PD = 0; PD < 8; PD++){
    digitalWrite(PD, LOW);
  }
  delay(1000);
}

int main(void){

  init();

  setup();
  //無限ループ
  while(1){
    light();
  }
  
  return 0;
  
}
