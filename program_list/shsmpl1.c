/* shsmpl1.c */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>

#define CHOICEBUFSIZ 128 //選択肢入力用の文字列バッファ

int main(void) {
    char chbuf[CHOICEBUFSIZ]; //選択肢入力用バッファ
    int choice, status;

    while(1) {
        printf("\n");
        printf("1. cal\n");
        printf("2. ls\n");
        printf("3. ps\n");
        printf("9. exit\n");
        printf("choose number : ");
        fgets(chbuf, CHOICEBUFSIZ, stdin);
        printf("\n");
        choice = atoi(chbuf);

        if(choice == 9){
            exit(0);
        }
        if(fork() == 0){
      switch(choice) {
        case 1 :
	        execl("/usr/bin/cal", "cal", NULL);
      	  break; //exec() 失敗用
        case 2 :
	        execl("/bin/ls", "ls", NULL);
	        break; //exec() 失敗用
        case 3 :
	        execl("/bin/ps", "ps", NULL);
	        break; //exec() 失敗用
      }
      fprintf(stderr, "cannot execute command\n");
      exit(1); //exec() 失敗時はすぐに終了する
    }
    else {
      wait(&status);
    }
  }
  return 0;
}