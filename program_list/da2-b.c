/**********************************************************************/
/* 課題２：「有理数を表す抽象データ型の作成」                         */
/**********************************************************************/
#include <stdio.h>
#include <stdlib.h>             /* abs 使用のため */

/**********************************************************************/
/* 構造体 rational の宣言                                             */
/**********************************************************************/
typedef struct rational {
    double n;
    double d;
   /* 構造体メンバの宣言 */

} ra_t;

/**********************************************************************/
/* 関数のプロトタイプ宣言                                             */
/**********************************************************************/
int   gcd(int p, int q);        /* 最大公約数 */

ra_t  raCon(int n, int d);      /* 'n/d' に相当する有理数を返す    */
ra_t  raAdd(ra_t a, ra_t b);    /* 有理数の加算：a と   b を足す   */
ra_t  raSub(ra_t a, ra_t b);    /* 有理数の減算：a から b を引く   */
ra_t  raMul(ra_t a, ra_t b);    /* 有理数の乗算：a と   b を掛ける */
ra_t  raDiv(ra_t a, ra_t b);    /* 有理数の除算：a を   b で割る   */
int   raCmp(ra_t a, ra_t b);    /* 有理数の大小比較                */
int   raFloor(ra_t a);          /* 有理数 a を上回らない最大の整数 */
void  raPrint(ra_t a);          /* 有理数を 'n/d' の形式で印字     */

/**********************************************************************/
/* メインプログラム                                                   */
/**********************************************************************/
int main(void) {

   ra_t p, q, r;

   printf("raCon( 210,  378) = "); raPrint(raCon( 210,  378));
   printf("raCon(-210,  378) = "); raPrint(raCon(-210,  378));
   printf("raCon( 210, -378) = "); raPrint(raCon( 210, -378));
   printf("raCon(-210, -378) = "); raPrint(raCon(-210, -378));

   p = raCon(6, 9);
   q = raCon(-9, 4);
   printf("p = "); raPrint(p);
   printf("q = "); raPrint(q);

   printf("p + q = "); raPrint(raAdd(p, q));
   printf("p - q = "); raPrint(raSub(p, q));
   printf("p * q = "); raPrint(raMul(p, q));
   printf("p / q = "); raPrint(raDiv(p, q));
   printf("p / p = "); raPrint(raDiv(p, p));

   r = raCon(7, 3);
   printf("r = "); raPrint(r);

   printf("p と p の比較結果: %d\n", raCmp(p, p));
   printf("p と q の比較結果: %d\n", raCmp(p, q));
   printf("p と r の比較結果: %d\n", raCmp(p, r));

   printf("raFloor(p) = %d\n", raFloor(p));
   printf("raFloor(q) = %d\n", raFloor(q));
   printf("raFloor(r) = %d\n", raFloor(r));

   return 0;

}

/**********************************************************************/
/* 有理数の操作関数の定義                                             */
/**********************************************************************/
int gcd(int p, int q) {
   if (q == 0)
       return p;
   else
       return gcd(q, p % q);
}

ra_t raCon(int n, int d) {
    int k;
    ra_t num;

    if(d < 0){
        n = n * -1;
        d = d * -1;
    }

    k = gcd(abs(n), d);
    num.n = n / k;
    num.d = d / k;

    return num;
}

ra_t  raAdd(ra_t a, ra_t b){
    ra_t Add;

    Add = raCon(a.n * b.d + b.n * a.d, a.d * b.d);

    return Add;
}

ra_t  raSub(ra_t a, ra_t b){
    ra_t Sub;

    Sub = raCon(a.n * b.d - b.n * a.d, a.d * b.d);

    return Sub;
}

ra_t  raMul(ra_t a, ra_t b){
    ra_t Mul;
    
    Mul = raCon(a.n * b.n, a.d * b.d);

    return Mul;
}

ra_t  raDiv(ra_t a, ra_t b){
    ra_t Div;

    Div = raCon(a.n * b.d, a.d * b.n);

    return Div;
}

int   raCmp(ra_t a, ra_t b){
    double Cmp_a, Cmp_b;

    Cmp_a = a.n * b.d;
    Cmp_b = b.n * a.d;

    if(Cmp_a > Cmp_b){
        return 1;
    }

    if(Cmp_a == Cmp_b){
        return 0;
    }

    if(Cmp_a < Cmp_b){
        return -1;
    }

    return 0;
}

int   raFloor(ra_t a){
    int Floor_a;

    Floor_a = a.n / a.d;

    if(Floor_a >= 0){
        return Floor_a;
    }

    else{
        return Floor_a - 1;
    }
}

void  raPrint(ra_t a){
    printf("%3.0f /%3.0f\n", a.n, a.d);
}