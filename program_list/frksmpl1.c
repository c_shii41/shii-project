/* frksmpl1.c */

#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>

int main(void){
  pid_t pid; //fork()からの返り値
  int i;
  /*　子プロセスを作る  */ 
  pid = fork();
  /* 返り値によって自分が親か子か判断する  */
  if(pid == -1){ //fork()失敗
    fprintf(stderr, "fork has failed.\n");
  }
  else if(pid == 0){ //子プロセス
      printf("I am a child process\n");
      printf("My PID  = %d\n", getpid()); //自分のPID
      printf("parent PID = %d\n", getppid()); //親のPID
      sleep(2);
  }
  else{  //親プロセス
      printf("I am a parent process.\n");
      printf("My PID  = %d\n", getpid()); //自分のPID
      printf("child PID = %d\n", pid); //子のPID
      sleep(3);
  }

  return 0;
}
