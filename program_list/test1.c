/*課題1 Hirochika Tanikawa 2017-04-18(Tue) 学籍番号16231040 */
#include<stdio.h>
#include<stdlib.h>  //atoiを使うためにインクルード
#include<ctype.h>   //isdigitを使うためにインクルード
#include "stack.h"  //stack.cを使うため

int ERR = 0; //エラーフラグ　0で正常　1でエラー


//char型で置かれた文字を数字に変換する関数
int num1(char n)
{
	return atoi(&n);
}
//式から数字の値を抽出する関数
int number(char *expr, int *next)
{
	int value;
	if(! isdigit(expr[*next]))
	{
		exit(1);
	}
	else
	{
		value = num1(expr[*next]);
		(*next) += 1;
		while(isdigit(expr[*next]))
		{
			value = value * 10 + num1(expr[*next]);
			(*next) += 1;
		}
	}
	return value;
}

//スタックが空であるかどうかを確かめ、空である場合はプログラムを強制終了し、それ以外の場合はなにもしない関数


//エラーの場合にエラーフラグをたて、引数に与えられたメッセージを表示する関数
void error(char *m)
{
        ERR = 1;
        fprintf(stderr, "%s", m);
}

//式の計算をし、計算結果を返す関数
int ValPolish(char *expr)
{
	int a,b,ans;
	int dest = 0,co = 1;
	stack st;
	makenull(&st);
	while(1)
	{
		//printf("ERR = %d\n",ERR);
		if( isdigit(expr[dest]) && ERR == 0 && co == 1) //数字の場合は、数字をスタックに入れる
		{
			co = 0;
			push(number(expr,&dest),&st);
		}
		else if(expr[dest] == '+' && ERR == 0 && co == 1) //2つの数の足し算をする
		{
			co = 0;
			if(!isempty(&st))
			{
				a = pop(&st);
			}
			else
			{
				error("空スタックからpopしようとしています。");
				return 0;
			}
			if(!isempty(&st))
			{
				b = pop(&st);
			}
			else
			{
				error("空スタックからpopしようとしています。");
				return 0;
			}
			push(a + b,&st);
			dest += 1;
		}
		else if(expr[dest] == '-' && ERR == 0 && co == 1) //後からpopした値から前にpopした値を引く
		{
			co = 0;
			if(!isempty(&st))
			{
				a = pop(&st);
			}
			else
			{
				error("空スタックからpopしようとしています。");
				return 0;
			}
			if(!isempty(&st))
			{
				b = pop(&st);
			}
			else
			{
				error("空スタックからpopしようとしています。");
				return 0;
			}
			push(b - a,&st);
			dest += 1;
		}
		else if(expr[dest] == '*' && ERR == 0 && co == 1) //2つの数の掛け算をする
		{
			co = 0;
			if(!isempty(&st))
			{
				a = pop(&st);
			}
			else
			{
				error("空スタックからpopしようとしています。");
				return 0;
			}
			if(!isempty(&st))
			{
				b = pop(&st);
			}
			else
			{
				error("空スタックからpopしようとしています。");
				return 0;
			}
			push(a * b,&st);
			dest += 1;
		}
		else if(expr[dest] == '/' && ERR == 0 && co == 1) //2つの数の割り算をする
		{
			co = 0;
			if(!isempty(&st))
			{
				a = pop(&st);
			}
			else
			{
				error("空スタックからpopしようとしています。");
				return 0;
			}
			if(!isempty(&st))
			{
				b = pop(&st);
			}
			else
			{
				error("空スタックからpopしようとしています。");
				return 0;
			}
			push(b / a,&st);
			dest += 1;
		}
		else if(expr[dest] == ',') //","記号は","の数を数えて文法が正しいかチェックする
		{
			//始点がコンマ記号の場合はエラーメッセージ表示
			if(dest == 0)
			{
				error("始点が','記号になっています。");
				return 0;
			}
			co += 1;
			dest += 1;
		}
		else if(expr[dest] == 'm' && ERR == 0 && co == 1) // 値の正負を逆にする
		{
			co = 0;
			if(!isempty(&st))
			{
				a = pop(&st);
			}
			else
			{
				error("空スタックからpopしようとしています。");
				return 0;
			}
			a *= -1;
			push(a,&st);
			dest += 1;
		}
		else if(expr[dest] == '\n' && ERR == 0 && co == 0)
		{

			if(!isempty(&st))
			{
				ans = pop(&st);
			}
			if(isempty(&st))
			{
				return ans;
			}
			else
			{
				error("式の入力方法が間違っています。\n"); //文末になっても式が2つ以上ある場合はエラーを返す
				printf("計算記号の数が少なすぎます。");
				return 0;
			}
		}
		//空白記号が来た場合もエラー
		else if(expr[dest] == ' ')
		{
			error("空白記号が入っています。\n");
			return 0;
		}
		else //エラー処理
		{
			if(expr[dest] == '\n' && co == 1) //終点にコンマがあるパターン
			{
				error("','記号の位置を確認してください。");
			}
			else if(co == 0) //区切り記号にコンマを使わなかったパターン
			{
				error("数字や式の間に','記号が使われていません。\n");
				printf("数字や式の区切りの間には','記号を使用してください。");
			}
			else if(co >= 2) //区切りにコンマを2つ以上使ったパターン
			{
				error("数字や式の間に','記号が多すぎます。");
			}
			else //異常な文字が入った場合などのパターン
			{
				error("使用できない記号が含まれています。");
			}
			return 0;
		}
	}
}


//正しい引数入力かを確認し、計算結果を表示するメインプログラム
int main()
{
	int total;
	char value[200];
	while(1)
	{
		ERR = 0;
		printf("計算したい逆ポーランド式を入力してください。\n");
		printf("何も入力せずにEnterキーを押すとプログラムを終了します。\n");
		fgets(value,200, stdin);



		if(value[0] != '\n')
		{
			total = ValPolish(value);
		}




		if(value[0] == '\n')
		{
			printf("式が入力されなかったのでプログラムを終了します。\n");
			return 0;
		}
		else if(ERR == 0)
		{
			printf("計算結果: %4d\n",total);  //計算結果を表示する

		}
		else
		{
			printf("もう一度正しい式を入力してください。\n");
		}
	}
}
