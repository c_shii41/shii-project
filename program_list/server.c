#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>

#define SOCKET "/tmp/xxx"

int gcd(int x, int y) {
  int t;
  while (x > 0) {
    if (x < y) { t = x; x = y; y = t; }
    x = x - y;
  }
  return y;
}

main()
{
  int i, s, t, len, x, y;
  struct sockaddr_un saddr;
  struct sockaddr_un caddr;
  char   buf[100];

  if ((s = socket(PF_UNIX, SOCK_STREAM, 0)) < 0) {
    perror("socket");
    exit(1);
  }

  saddr.sun_family = AF_UNIX;
  strcpy(saddr.sun_path, SOCKET);

  unlink(SOCKET);
  if (bind(s, (struct sockaddr *)&saddr, sizeof(struct sockaddr_un)) < 0) {
    perror("bind");
    exit(1);
  }

  if (listen(s,1) < 0) {
    perror("listen");
    exit(2);
  }

  for (;;) {
    len = sizeof(caddr);
    if ((t = accept(s, (struct sockaddr *)&caddr, &len)) < 0) {
      perror("accept");
      exit(3);
    }

    while (len = read(t, buf, 100)) {
      sscanf(buf, "%d %d", &x, &y); 
      sprintf(buf, "gcd(%d,%d)=%d\n", x, y, gcd(x,y));
      write(t, buf, 100);
    }
    close(t);
  }
}
