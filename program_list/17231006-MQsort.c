/*
[課題]
高速なソート法（クイックソートとマージソート）について、プログラムを完成し、ソート時間に関する実験を行い、レポートにまとめること。
素朴なソート法のプログラムは含めなくてよい。ただし、考察では素朴な方法との比較を含めるように。

レポートの最低条件として以下を満たしていることを確認してから提出すること．

１. ソートの時間とデータ数についての両対数グラフがあること．

・マージソート
・クィックソート
・qsort

（データの個数 10^5 〜 10^8 程度のデータで確認していること）

２．グラフとオーダーの関係について考察があること

３. ソートの速度の違いについて考察があること．

４. データの疑似乱数の範囲によるソート時間の違いついて、実験と考察があること
*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

//配列の大きさ
#define max_size 10000
#define max_element 10000
typedef int element_type; //データの型


void rand_create(int data[], int i){
  int j;

  for(j = 0; j < i; j++){
    data[j] = random() % max_element;
  }
}

void print_Array(int x[], int n){
  int i;

  for(i = 0; i < n; i++){
    printf("%d ",x[i]);
  }
  printf("\n");
}

//バブル法
void bubble_sort(element_type data[], int n){
  int i, j, temp;
  
  for(i = 0; i < n - 1; i++){
    for(j = n - 1; j > i; j--){
      if(data[j - 1] > data[j]){
	      temp = data[j - 1];
	      data[j - 1] = data[j];
	      data[j] = temp;
      }
    }
  }
}

//選択法
void selection_sort(element_type data[], int n){
  int i, j, temp;
  
  for(i = 0; i < n - 1; i++){
    for(j = i + 1; j < n; j++){
      if(data[i] > data[j]){
	      temp = data[i];
	      data[i] = data[j];
	      data[j] = temp;
      }
    }
  }
}

//挿入法
void insertion_sort(element_type data[], int n){
  int i, j;
  element_type temp;
  
  for(i = 1; i < n; i++){
    temp = data[i];
    for(j = i; j > 0 && data[j - 1] > temp; j--){
      data[j] = data[j - 1];
    }
    data[j] = temp;
  }
}

int compare_int(const void *a, const void *b)
{
    return *(int*)a - *(int*)b;
}

int find_pivot(element_type data[], int i, int j){
  int k;

  for(k = i + 1; k <= j; k++){
    if(data[k] != data[i]){
      if(data[i] > data[k]){
	return i;
      }
      else
	return k;
    }
  }
  return -1;
}

int partition(element_type data[], int i, int j, element_type pivot){
  int left, right, temp;

  left = i;
  right = j;
  do{
    while(data[left] < pivot){
      left++;
    }
    while(data[right] >= pivot){
      right--;
    }
    if(left < right){
      temp = data[left];
      data[left] = data[right];
      data[right] = temp;
    }
  }while(left <= right);
  return left;
}

//クイックソート
void quick_sort(element_type data[], int i, int j){
  int pivotindex, k;

  pivotindex = find_pivot(data, i, j);
  if(pivotindex >= 0){
    k = partition(data, i ,j, data[pivotindex]);
    quick_sort(data, i, k - 1);
    quick_sort(data, k , j);
  }
}

void Merge(int data[], int left, int mid, int right){
  int n1 ,n2 , i, j, k, count = 0;
  int *L ,*R;
  n1 = mid - left;
  n2 = right - mid;
  L = (int *)malloc(sizeof(int)*(n1+1));
  R = (int *)malloc(sizeof(int)*(n2+1));
  for(i = 0; i <= n1 - 1; i++){
    L[i] = data[left+i];
  }
  for(j = 0;j <= n2 - 1; j++){
    R[j] = data[mid+j];
  }
  L[n1] = 102400000;
  R[n2] = 102400000;
  i = 0;
  j = 0;
  for(k = left; k <= right - 1; k++){
   if(L[i] <= R[j]){
      data[k] = L[i];
      i++;
      count++;
    }
    else{
      data[k] = R[j];
      j++;
      count++;
    }
  }
  free(L);
  free(R);
}
//マージソート
void Merge_sort(element_type data[], int left, int right){
    int mid;

    if(left + 1 < right){
      mid = (left + right) / 2;
      Merge_sort(data, left, mid);
      Merge_sort(data, mid, right);
      Merge(data, left, mid, right);
    }
}

int main(void){
    clock_t start_time, end_time;
    double cpu_time;

    int i = max_size;

    for (;i < 50000;){
      int *data;
      data = (int*)malloc(sizeof(int)*i);
      srandom(time(NULL));
      if(i == 10000){
        printf("    要素数  バブルソート  選択ソート  挿入ソート\n");
      }
        printf("%10d   ", i);
        printf(" 処理中 ");
        fflush( stdout );
                
        start_time = clock();
        rand_create(data, i);
        bubble_sort(data, i);
        //print_Array(data, i);
        end_time = clock();
        cpu_time = (double)(end_time - start_time) / CLOCKS_PER_SEC;
        printf(" \b\b\b\b\b\b\b\b\b\b\b\b%10.5f s   ", cpu_time);
        printf("  処理中 "); 
        fflush( stdout );
        
        start_time = clock();
        rand_create(data, i);
        selection_sort(data, i);
        //print_Array(data, i);
        end_time = clock();
        cpu_time = (double)(end_time - start_time) / CLOCKS_PER_SEC;
        printf("\b\b\b\b\b\b\b\b\b\b\b%10.5f s   ", cpu_time);
        printf(" 処理中 "); 
        fflush( stdout );

        start_time = clock();
        rand_create(data, i);
        insertion_sort(data, i);
        //print_Array(data, i);
        end_time = clock();
        cpu_time = (double)(end_time - start_time) / CLOCKS_PER_SEC;
        printf("\b\b\b\b\b\b\b\b\b\b\b%10.5f s\n", cpu_time);
	      //printf(" 処理中 ");
        fflush( stdout );
        free(data);
	i = i + 5000;
    }

      for(; i >= 50000;){
        int *data;
        data = (int*)malloc(sizeof(int)*i);
        srandom(time(NULL));
        if(i == 100000){
          printf("    要素数  クイックソート　 qsort	 Merge\n");
        }
        printf("%10d   ", i);
        printf(" 処理中 ");
        fflush( stdout );

	      start_time = clock();
        rand_create(data, i);
        quick_sort(data, 0, i);
        //print_Array(data, i);
        end_time = clock();
        cpu_time = (double)(end_time - start_time) / CLOCKS_PER_SEC;
        printf(" \b\b\b\b\b\b\b\b\b\b\b%10.5f s   ", cpu_time);
	      printf(" 処理中 ");
        fflush( stdout );
	
	      start_time = clock();
	      rand_create(data, i);
	      qsort(data,i , sizeof(int), compare_int);
	      //print_Array(data, i);
	      end_time = clock();
	      cpu_time = (double)(end_time - start_time) / CLOCKS_PER_SEC;
	      printf("\b\b\b\b\b\b\b\b\b\b%10.5f s ", cpu_time);
        printf("  処理中"); 
        fflush( stdout );

	      start_time = clock();
        rand_create(data, i);
        Merge_sort(data, 0, i);
        //print_Array(data, i);
        end_time = clock();
        cpu_time = (double)(end_time - start_time) / CLOCKS_PER_SEC;
        printf(" \b\b\b\b\b\b\b\b\b\b%10.5f s\n", cpu_time);
        //printf(" 処理中 "); 
        fflush( stdout );
        free(data);

	i = i * 2;

	if(i > 102400000){
	  return 0;
	}
	
  }
    return 0;
}

/*実行結果
(乱数範囲0 ~ 9999)
	要素数　バブルソート	  選択法	    挿入法
	10000	  0.46225s	 0.45394s	  0.12719s
	15000	  1.06979s	 0.97876s	  0.28996s
	20000	  1.93680s	 1.65163s	  0.51646s
	25000	  3.15538s	 2.44360s	  0.79465s
	30000	  4.44218s	 3.31886s	  1.18569s
	35000	  6.04272s	 4.30052s	  1.57847s
	40000	  7.96405s	 5.50885s	  2.12595s
	45000	 10.05710s	 6.51952s	  2.60974s
	50000	 12.47882s	 7.77544s	  3.26228s
	55000	 15.12077s	 9.09291s	  3.91903s
	60000	 18.06642s	10.60296s	  4.61662s
	65000	 21.17872s	12.01694s	  5.44650s
	70000	 24.78109s	13.62851s	  6.48929s
	75000	 28.25061s	15.31212s	  7.35930s
	80000	 32.20647s	16.57597s	  8.30405s
	85000	 36.52030s	18.87041s	  9.32637s
	90000	 40.92942s	21.03524s	 10.26790s
	95000	 45.54616s	22.99644s	 11.76708s
	要素数        クイック	  q_sort	    マージ
	   100000	  0.01920s   0.02516s	  0.04066s
	   200000	  0.03778s	 0.05143s	  0.08224s
	   400000	  0.07182s	 0.10559s	  0.16939s
	   800000	  0.14363s	 0.21613s	  0.35515s
	  1600000	  0.28829s   0.44411s	  0.71126s
	  3200000	  0.57337s	 0.91300s	  1.45896s
	  6400000	  1.12746s	 1.84798s	  2.96242s
	 12800000	  2.22266s	 3.74597s	  6.10895s
	 25600000	  4.44984s	 7.67608s	 12.49448s
	 51200000	  8.92747s	15.76459s	 25.68450s
	102400000	 18.07570s	32.43474s	 52.20320s

(乱数範囲0 ~ 99)
要素数	  バブル	     選択法	    挿入法
10000	  0.43971s	 0.19191s	  0.13324s
15000	  1.10235s	 0.42307s	  0.29222s
20000	  1.87653s	 0.75200s	  0.51565s
25000	  2.94765s	 1.18092s	  0.80638s
30000	  4.26157s   1.67553s	  1.13977s
35000	  5.83792s	 2.29352s	  1.55756s
40000	  7.63917s	 2.97619s	  2.03736s
45000	  9.73542s	 3.82891s	  2.61186s
50000	 12.13548s	 4.69353s	  3.18810s
55000	 14.64348s	 5.64262s	  3.90324s
60000	 17.40124s	 6.72435s	  4.61132s
65000	 20.59058s	 7.90369s	  5.41115s
70000	 23.95913s	10.57453s	  7.31489s
80000	 31.40472s	12.00515s	  8.31519s
85000	 35.27584s	13.45097s	  9.32250s
90000	 39.67631s	15.16540s	 10.55925s
95000	 44.47876s	16.90359s	 11.62187s
要素数        クイック	  q_sort	    マージ
   100000	  0.00984s   0.01997s	  0.03865s
   200000	  0.01995s   0.04087s	  0.07791s
   400000	  0.03901s   0.08628s	  0.15750s
   800000	  0.07810s   0.16913s	  0.32602s
  1600000	  0.15292s   0.34957s	  0.67029s
  3200000	  0.30051s   0.73347s	  1.36461s
  6400000	  0.60471s   1.47257s	  2.85026s
 12800000	  1.22590s   3.05457s	  5.83099s
 25600000	  2.43884s   6.28898s	 11.67624s
 51200000	  4.92589s  13.01600s	 23.33930s
102400000	  9.78585s  26.27846s	 49.58429s
*/

/*
○グラフとオーダーの関係についての考察
  レポートに示したマージソート、クイックソート、ライブラリ関数のクイックソートの両対数グラフの近似直線は、いずれも右上がりの直線グラフとなった。
両対数グラフが直線であることは両対数グラフをとる前の関数が、べき関数 y = ax^n(a > 0)で表されることを意味している。
試しに、べき関数 y = ax^nにおいて両辺の対数を取ると、logy = nlogx + logaとなり、今回の課題ではx = nであることからオーダーO(nlogn)となることが予測できる。
ただし、この場合logaは非常に小さいものとして省略可能である。
実際にExcelの近似直線の項は、すべてべき関数で表されており、理論に基づいていることがわかる。

  また、レポートに示したバブルソート、選択法、挿入方の両対数グラフの近似直線はいずれも右上がりのほぼ直線グラフとなった。
しかし、クイックソートなどと異なり単純ソートは両対数グラフにおいて完全な直線ではない。
Excelの近似直線は二次関数 y = ax^2 + bx + c(a,b,c:実数)で表されており、オーダーO(n^2)となるからである。


○ソート速度の違いについての考察
　・単純ソートについて
　　得られた実行結果から、単純に比較するとソート速度は遅い方から順にバブルソート < 選択法 < 挿入法となった。
　このソート速度の差の原因は代入回数に違いがあるからである。
　バブルソートでは(n - 1)個のデータ数をtempも含め3回代入を行うことになるので3(n - 1)回代入を行う。
　挿入法ではfor文の2重ループで(n - 1)回、ループ部分で2重ループ前後に1回ずつ行うのでそれぞれの和の(n + 1)回行う。
　選択法ではバブルソートと酷似しているが、バブルソートは総当たりで代入するのに対して選択法は配列の最小値を持つ要素を探して先頭要素の代入を行うので
　代入回数はバブルソートよりも少ない。しかし、挿入方よりは多くなる。
　従って単純ソートのソート速度は遅い方から順にバブルソート < 選択法 < 挿入法となる。
　また、それぞれのソート方法において約2倍の差が生じていることもわかる。swap方法の違いによって倍数差が生じていることも結果によって明かになった。

　・高速ソートについて
　　マージソート、クイックソートともに単純ソートに比べれば圧倒的に高速なソーティングだと言えるが、ライブラリ関数のクイックソートを含めたこれら
　3つのソート方法においても差が生じている。
  ライブラリ関数のクイックソートとクイックソートの間に差が生じている理由はライブラリ関数のクイックソートはクイックソートに比べて比較毎に比較関数を
　呼び出しているからである。
　また、マージソートとクイックソートの間に差が生じている理由はマージソートの方がクイックソートよりもコピーする回数が多いからである。これは2本の配列で
　マージしながら交互にマージすることで多少は改善されるが、それでもクイックソートに及ぶには至らないという結果になった。

○データの疑似乱数の範囲によるソート時間の違いの考察
　今回の実験は最初に乱数の範囲を0 ~ 9999の範囲で行い、次に0 ~ 99の範囲で行った。
　・単純ソートについて
　上記の考察どおり、バブルソートと挿入方は代入回数が一定なので、乱数重複があってもソート時間はほとんど変わりない。
　しかし、選択法では乱数重複が増えるほど代入回数が減少するのでソート速度が速くなった。
　・高速ソートについて
　3つの高速ソートのうち、一番高速化したのはクイックソートで疑似乱数範囲0 ~ 9999の時と比べて約半分のソート時間となった。
　乱数重複が増えたことで代入回数が減少、pivotの分割が理想状態に近づいたために高速化されたと考えられる。
　他の2つの高速ソートもある程度は高速化されているが数秒程度でありライブラリ関数のクイックソートとは約3倍、マージソートとは約5倍もの差が開いてしまっている。
　乱数重複の多い数列のソートではクイックソートがいかに強力であるかがはっきりとわかる。
*/
