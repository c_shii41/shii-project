#include <stdio.h>
#include <stdlib.h>
#define STACKSIZE 100

typedef struct{
	int kind;
	int line;
	int pos;
} elem_t;

typedef struct{
	elem_t data[STACKSIZE];
	int top;
} stack_t;

//*********************************************//

void initStack(stack_t *st);
void push(elem_t x, stack_t *st);
elem_t pop(stack_t *st);
elem_t peekStack(const stack_t *st);
int isEmptyStack(const stack_t *st);
int isFullStack(const stack_t *st);
void printStack(const stack_t *st);
int kind(char chara);

//*********************************************//

void initStack(stack_t *st){
   st->top = -1;
}

elem_t peekStack(const stack_t *st){
   if (isEmptyStack(st)) {
       printf("PeekStack Error: Stack Empty\n");
       exit(1);
   }
   return st->data[st->top];
}

void printStack(const stack_t *st) {
   int i;

   printf("Stack = (");
   for (i = 0; i <= st->top; i++) {
       printf("%c ", st->data[i]);
   }
   printf(")\n");
}

void push(elem_t x, stack_t *st){
   st -> top++;
   st -> data[st -> top] = x;
}

elem_t pop(stack_t *st){
   elem_t c;

   c = st -> data[st -> top];
   st -> top--;
	
   return c;
}

int isEmptyStack(const stack_t *st){
   if(st -> top == -1){
	return 1;
   }
   else
	return 0;
}

int isFullStack(const stack_t *st){
   if(st -> top == STACKSIZE - 1){
	return 1;
   }
   else
	return 0;
}

int kind(char chara){
 int code;
 switch (chara) {
 case '(':
   code = 1;
   break;
 case ')':
   code = 2;
   break;
 case '[':
   code = 3;
   break;
 case ']':
   code = 4;
   break;
 default:
   code = 0;
   break;
 }
 return code;
}


//*********************************************//

int main(){
   char c;
   stack_t st;
   int line = 1;
   int pos = 0;
   int i,k,a,b;
   elem_t x,temp;

   a = 0;
   b = 0;

   initStack(&st);

   printf("input line : ");

   while((c = getchar()) != 'EOF'){
	if(c == '\n'){
	    line++;
	    pos = 0;
	    continue;
	}
   pos++;

   k = kind(c);
   if(k > 0){                      /* 文字が括弧である */
	if(k % 2 == 1){             /* 括弧が開き括弧である */
	    if(k == 1){             /* 開き括弧の種類が "("である */
		a = a + 1;
	    }
	    else if(k == 3){        /* 開き括弧の種類が "["である */
       	b = b + 1;
	    }
	x.kind = k;
	x.line = line;
	x.pos  = pos;
	push(x,&st);
       }

       else if((a > 0 && k == 2) || (b > 0 && k == 4){
	    if(k == 2){
	        a = a - 1;
	    }
	    else if(k == 4){
	        b = b - 1;
           }
	    temp = pop(&st);        /* 対応する閉じ括弧がある */
       }

       else{
	    printf("%d行目の%d文字目に対応する開き括弧がありません。\n", line,
pos);
       }
   }

   puts("Moziretsu no saigo");
   if(!isEmptyStack(&st)){
	printf("taiousuru tozikakko ga nai.");
   }

   while(isEmptyStack(&st) == 0){
	x = pop(&st);
	printf("%d gyoumeno %d mozime.\n", x.line, x. pos);
   }
}
   return 0;

   }