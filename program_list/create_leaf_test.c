/*  malloc 関数の初期値確認プログラム 
malloc 関数は確保した領域の初期化を行いません。create_leaf() で葉ノードを
確保した場合、leftchild, rightchild が NULL であるとは限りません。
*/

#include <stdio.h>
#include <stdlib.h>

typedef struct _node{
  enum {LEAF,INTERNAL} kind;
  int value; 
  struct _node *leftchild,*rightchild;
  char operator; 
}node, *tree;

tree create_leaf(int v){
  tree t;
  t = (tree)malloc(sizeof(node));
  t -> kind = LEAF;
  t -> value = v;
  return t;
}

tree create_internal(char op, tree sub1, tree sub2) {
  tree t;
  t = (tree)malloc(sizeof(node));
  t->kind = INTERNAL;
  t->operator = op;
  t->leftchild = sub1;
  t->rightchild = sub2;
  return t;
}

int main(){
  tree t;

  t = create_leaf(100);

  printf("t = %p\n",t);
  printf("t->leftchild = %p\n",t->leftchild);

  free(t);

  t = create_internal('-',create_leaf(30),NULL);  
  printf("t = %p\n",t); 
  printf("t->leftchild = %p\n",t->leftchild); 
 
  free(t->leftchild);
  free(t);

  t = create_leaf(100);

  if (t->leftchild != NULL) {
    printf("t = %p\n",t);
    printf("t->leftchild = %p\n",t->leftchild); // 前の値が残っている
  }

  return 0;
}
