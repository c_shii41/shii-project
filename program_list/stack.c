#include "stack.h"

/* 関数の定義　*/

void makenull(stack *s){
  s->top = -1;
}

void push(int x, stack *s){
  s->top++;
  s->elements[s->top] = x;
}

int pop(stack *s){
  int c;

  c = s->elements[s->top];
  s->top--;

  return c;
}

int isempty(stack *s){
  if(s->top == -1){
    return 1;
  }
  else
    return 0;
}

  
