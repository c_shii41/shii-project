#include <WProgram.h>

int ledPin = 4, switchPin = 6;
int light = 0;
int old = 0;

//4番を出力、6番を入力に設定
void setup(){
  pinMode(ledPin, OUTPUT);
  pinMode(switchPin, INPUT);
}

void loop(){
  int in;
  while(1){
    in = digitalRead(switchPin); //現状態
    if(in == 1 && old == 0){ //押された瞬間
      light = !(light);
      digitalWrite(ledPin, light); //出力
      delay(20); //チャタリング対策
    }
    old = in; //前状態の保持
  }
}

int main(void){
  init();

  setup();
  
  loop();

  return 0;
}
